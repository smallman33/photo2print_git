Django==1.7.0

gunicorn==19.6.0

psycopg2==2.6.1

pillow==4.0.0

python-dateutil==2.6.0

django-suit==0.2.24