#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''

#######################################################################
# Class Declare
#######################################################################


class  ChoiceException(Exception):
    ''' Exception of Passbook class
    '''
    def __init__(self, value):
        super(ChoiceException, self).__init__()
        self.value = value

    def __str__(self):
        return repr(self.value)
