# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import json
import os
from choiceException import ChoiceException
import collections

class Choice(object):
    '''
    '''
    def __init__(self, choiceJSONFilePath):
        '''
        '''
        print choiceJSONFilePath
        if not os.path.exists(choiceJSONFilePath):
            raise ChoiceException( "JSON Path was not exists." )
        self.choiceJSONFilePath = choiceJSONFilePath
        self.choiceDict = dict()
        self.rawChoiceDice = dict()

    @staticmethod
    def convertChoiceDictToDjangoFromFormat( jsonDict ):
        '''
        '''
        outputDict = dict()
        for key, var in jsonDict.iteritems():
            outputDict[str(key)] = tuple()
            varSorted = collections.OrderedDict(sorted(var.items()))
            for subKey, subVar in varSorted.iteritems():
                outputDict[str(key)] += ((str(subKey), str(subVar)),)
        return outputDict

    @staticmethod
    def convertdjangoFromFormatToChoiceDict( jsonDict ):
        '''
        '''
        outputDict = dict()
        for key, var in jsonDict.iteritems():
            outputDict[str(key)] = dict()
            for vatTuble in var:
                outputDict[str(key)][vatTuble[0]] = vatTuble[1]
        return outputDict

    def getChoiceDict(self):
        '''
        '''
        return self.choiceDict

    def load(self):
        '''
        '''
        data_loaded = None
        # Read JSON file
        with open(self.choiceJSONFilePath) as InputJSONfile:
            data_loaded = json.load(InputJSONfile)
        self.rawChoiceDice = data_loaded
        # Convert to django from format
        self.choiceDict = self.convertChoiceDictToDjangoFromFormat( data_loaded )

    def dump(self):
        '''
        '''
        tmpOutput = None
        tmpOutput = self.convertdjangoFromFormatToChoiceDict( self.choiceDict )
        # Write JSON file
        with open(self.choiceJSONFilePath, 'w') as OutputJSONfile:
            str_ = json.dumps(tmpOutput, indent=4, ensure_ascii=False)
            OutputJSONfile.write(str_)