# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''

from django.conf.urls import patterns, include, url
from Application import *
from Application.models import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls import *
#from django.conf.urls.defaults import *
#from django.views.generic import direct_to_template
admin.autodiscover()

urlpatterns = patterns(
	##################################################################
    ###	System
    ##################################################################
    '',
    url(r'^media/(?P<path>.*)$',
		'django.views.static.serve',
		{'document_root': settings.MEDIA_ROOT}),
	url(r'^admin/',
		include(admin.site.urls)),
    ##################################################################
    ###	General
    ##################################################################
	url(r'^$',
		'Application.views.Page_HomeStatusSingOut_domainUrl',
		name='home'),
	url(r'^HomePage/$',
		'Application.views.Page_HomeStatusSingOut',
		name='home'),
	url(r'^(?P<username>\S+)/HomePage/$',
		'Application.views.Page_HomeStatusSingIn',
		name='home'),
	##################################################################
	### Contact
	##################################################################
	url(r'^ContactPage/$',
		"Application.views.Page_GuestViewContact"),
	url(r'^(?P<username>\S+)/ContactPage/$',
		"Application.views.Page_AllUserViewContact"),
	##################################################################
	###	Manage User
	##################################################################
	url(r'^AllUserLoginFunction/$',
		'Application.views.Function_AllUserLogin'),
	url(r'^LoginPage/$',
		'Application.views.Page_GuestLogin',
		name="LoginPage"),
	url(r'^AllUserLogoutFunction/$',
		'Application.views.Function_AllUserLogout'),
	##################################################################
	### Register
	##################################################################
	url(r'^RegisterPage/$',
		'Application.views.Page_GuestRegister'),
	url(r'^RegisterFunction/$',
		'Application.views.Function_GuestRegister'),
	##################################################################
	### Delete User
	##################################################################
	url(r'^AdminDeleteUserFunction/$',
		'Application.views.Function_AdminDeleteUser'),
	##################################################################
	### Detail Member
	##################################################################
	url(r'^(?P<username>\S+)/SelfDetailPage/$',
		'Application.views.Page_MemberViewDetailSelf',
		name='SelfDetailPage'),
	url(r'^(?P<username>\S+)/EditUserDetail/(?P<usernamepost>\S+)/$',
		'Application.views.Page_AllUserEditDetailUser',
		name='EditUserDetail'),
	url(r'^EditUserDetailFunction/$',
		'Application.views.Fucntion_AllUserEditDetailUser'),
	url(r'^(?P<username>\S+)/AdminViewMemberListPage/$',
		'Application.views.Page_AdminViewMemberList',
		name='AdminViewMemberListPage'),
	##################################################################
	###	Gallery
	##################################################################
	url(r'^(?P<username>\S+)/GalleryPage/$',
		'Application.views.Page_MemberViewAlbums'),
	url(r'^(?P<username>\S+)/AddAlbumPage/$',
		'Application.views.Page_MemberAddAlbum'),
	url(r'^AddAlbumFunction/$',
		'Application.views.Function_MemberAddAlbum'),
	url(r'^DeleteAlbumFunction/$',
		'Application.views.Function_MemberDeleteAlbum'),
	url(r'^(?P<username>\S+)/AdminViewMemberAlbums/(?P<usernamepost>\S+)/$',
		'Application.views.Page_AdminViewMemberAlbums'),
	##################################################################
	### Manage Picture
	##################################################################
	url(r'^(?P<username>\S+)/GalleryPage/AlbumName/(?P<albumname>\S+)/PictureName/(?P<imagename>\S+)/$',
		'Application.views.Page_MemberViewFullPictures'),
	url(r'^(?P<username>\S+)/GalleryPage/AlbumName/(?P<albumname>\S+)/UploadPicturePage/$',
		'Application.views.Page_MemberUploadPicture'),
	url(r'^(?P<username>\S+)/GalleryPage/AlbumName/(?P<albumname>\S+)/$',
		'Application.views.Page_MemberViewPictures'),
	url(r'^UploadPictureFunction/$',
		'Application.views.Function_MemberUploadPicture'),
	url(r'^DeletePictureFunction/$',
		'Application.views.Fucntion_MemberDeletePicture'),
	url(r'^(?P<username>\S+)/AdminViewMemberPicture/(?P<usernamepost>\S+)/(?P<albumnamepost>\S+)/$',
		'Application.views.Page_AdminViewMemberPicture'),
	##################################################################
	### Admin Order
	##################################################################
	url(r'^(?P<username>\S+)/AdminViewMemberOrder/(?P<usernamepost>\S+)/$',
		'Application.views.Page_AdminViewMemberOrder'),
	url(r'^(?P<username>\S+)/AdminViewMemberOrderExtend/(?P<usernamepost>\S+)/(?P<date_time>\S+)/$',
		'Application.views.Page_AdminViewOrderListExtend'),
	url(r'^(?P<username>\S+)/AdminViewMemberConfirmOrderDetail/(?P<usernamepost>\S+)/(?P<date_time>\S+)/$',
		'Application.views.Page_AdminViewMemberConfirmOrderDetail'),
	url(r'^(?P<username>\S+)/AdminViewAllMemberOrder/$',
		'Application.views.Page_AdminViewAllMemberOrder'),
	url(r'^(?P<username>\S+)/AdminViewMemberOrderBySeleteOption/$',
		'Application.views.Page_AdminViewOrderListOption'),
	url(r'^AdminDeleteOrderFunction/$',
		'Application.views.Function_AdminDeleteOrder'),
	url(r'^AdminCheckPaidFunction/$',
		'Application.views.Function_AdminCheckPaid'),
	url(r'^AdminUncheckPaidFunction/$',
		'Application.views.Function_AdminUncheckPaid'),
	url(r'^AdminEditOrderStatusFunction/$',
		'Application.views.Function_AdminEditOrderStatus'),
	url(r'^(?P<username>\S+)/AdminEditOrderStatusPage/(?P<usernamepost>\S+)/(?P<date_time>\S+)/$',
		'Application.views.Page_AdminEditOrderStatus'),
	##################################################################
	###	Member Order
	##################################################################
	##### Images
	url(r'^MemberOrderPictureFunction/$',
		'Application.views.Function_MemberOrderImage'),
	url(r"^(?P<username>\S+)/OrderPage/AlbumName/(?P<albumnamepost>\S+)/PictureName/(?P<picturenamepost>\S+)/MemberSelectPaperTypePage/(?P<usernamepost>\S+)/$",
		'Application.views.Page_MemberOrderImageType'),
	url(r'^(?P<username>\S+)/OrderPage/AlbumName/(?P<albumnamepost>\S+)/PictureName/(?P<picturenamepost>\S+)/MemberSelectPaperSurfacePage/(?P<usernamepost>\S+)/$',
		'Application.views.Page_MemberOrderImageSurface'),
	url(r'^(?P<username>\S+)/OrderPage/AlbumName/(?P<albumnamepost>\S+)/PictureName/(?P<picturenamepost>\S+)/MemberSelectPaperSizeAndMarginPage/(?P<usernamepost>\S+)/$',
		'Application.views.Page_MemberOrderImageSizeAndMargin'),
	url(r'^(?P<username>\S+)/OrderPage/AlbumName/(?P<albumnamepost>\S+)/PictureName/(?P<picturenamepost>\S+)/MemberCheckOrderAndSubmit/(?P<usernamepost>\S+)/$',
		'Application.views.Page_MemberOrderImageCheck'),
	##### Albums
	url(r'^MemberOrderAlbumFunction/$',
		'Application.views.Function_MemberOrderAlbum'),
	url(r"^(?P<username>\S+)/OrderPage/AlbumName/(?P<albumnamepost>\S+)/MemberSelectPaperTypePage/(?P<usernamepost>\S+)/$",
		'Application.views.Page_MemberOrderAlbumType'),
	url(r'^(?P<username>\S+)/OrderPage/AlbumName/(?P<albumnamepost>\S+)/MemberSelectPaperSurfacePage/(?P<usernamepost>\S+)/$',
		'Application.views.Page_MemberOrderAlbumSurface'),
	url(r'^(?P<username>\S+)/OrderPage/AlbumName/(?P<albumnamepost>\S+)/MemberSelectPaperSizeAndMarginPage/(?P<usernamepost>\S+)/$',
		'Application.views.Page_MemberOrderAlbumSizeAndMargin'),
	url(r'^(?P<username>\S+)/OrderPage/AlbumName/(?P<albumnamepost>\S+)/MemberCheckOrderAndSubmit/(?P<usernamepost>\S+)/$',
		'Application.views.Page_MemberOrderAlbumCheck'),
	##### Other
	url(r'^(?P<username>\S+)/OrderPage/$',
		'Application.views.Page_MemberViewOrderSelf'),
	url(r'^(?P<username>\S+)/OrderPage/ExtendOrder(?P<date_time>\S+)/$',
		'Application.views.Page_MemberViewOrderExtend'),
	url(r'^(?P<username>\S+)/OrderPage/ConfirmOrder/(?P<date_time>\S+)/$',
		'Application.views.Page_MemberConfirmOrder'),
	url(r'^(?P<username>\S+)/OrderPage/ConfirmOrderDetail/(?P<date_time>\S+)/$',
		'Application.views.Page_MemberViewConfirmOrder'),
	url(r'^MemberConfirmOrderFunction/$',
		'Application.views.Function_MemberConfirmOrder'),
	##################################################################
	###	Download
	##################################################################
	url(r'^AllUserDownloadFileFunction/$',
		"Application.views.Function_AllUserDownloadFile"),
	url(r'^AllUserDownloadZipFileFunction/$',
		"Application.views.Function_AllUserDownloadZipFile"),
	
)