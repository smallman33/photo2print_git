# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from tempfile import *
import datetime
#from Application.forms import *
from Photo2Print.settings import *

def Path(instance, filename):
	return 'Account/%s/%s/%s' % (
		instance.album.user.user.username,
		instance.album.albumname,
		filename)


class UserProfile(models.Model):

	user = models.OneToOneField(
		User,
		unique=True)
	permission = models.CharField(
		max_length=1,
		choices=CHOICE_DICT["PERMISSION"],
		blank=True,
		null=True)
	quota = models.IntegerField(
		blank=True,
		null=True)
	activation_key = models.CharField(
		max_length=10)

	def create_user_profile(sender, instance, created, **kwargs):
		if created:
			UserProfile.objects.create(user=instance)

	post_save.connect(create_user_profile, sender=User)

	def create_user_activate(sender, instance, created, **kwargs):
		if created:
			UserActivate.objects.create(user=instance)

	post_save.connect(create_user_activate, sender=User)

class UserActivate(models.Model):
    user = models.OneToOneField(User, related_name='activation') #1 to 1 link with Django User
    activation_key = models.CharField(max_length=40)
    key_expires = models.DateTimeField()

class Album (models.Model):
	albumcover = models.CharField(
		max_length=150)
	albumname = models.CharField(
		max_length=150,
		blank=datetime.datetime.now())
	permissionalbum = models.CharField(
		max_length=2,
		choices=CHOICE_DICT["PERMISSIONALBUM"])
	datecreated = models.DateTimeField(
		auto_now_add=True)
	datelastmodify = models.DateTimeField(
		auto_now=True)
	comment = models.CharField(
		max_length=400)
	user = models.ForeignKey(UserProfile)
	def get_reviews(self, obj):
		return '%s' % (obj.user.username)

	get_reviews.short_description = 'username'


class Images(models.Model):
	imagename = models.CharField(
		max_length=150,
		blank=datetime.datetime.now())
	imagepath = models.FileField(
		upload_to=Path)
	width = models.IntegerField(
		blank=True,
		null=True)
	height = models.IntegerField(
		blank=True,
		null=True)
	dateupload = models.DateTimeField(
		auto_now_add=True)
	comment = models.CharField(
		max_length=400)
	filesize = models.IntegerField(
		blank=True,
		null=True)
	album = models.ForeignKey(Album)


class Log_order(models.Model):
	date_time = models.DateTimeField(
		auto_now_add=True)
	price_total = models.IntegerField(
		blank=True,
		null=True)
	price_perpic = models.CharField(
		max_length=3,
		choices=CHOICE_DICT["PEPER_PRICE_ALL"])
	payment_is_checked = models.BooleanField(
		blank=False)
	payment_is_confirm = models.BooleanField(
		blank=False)
	date_time_paid = models.DateTimeField(
		blank=True,
		null=True)
	amount_paid = models.FloatField(
		blank=True,
		null=True)
	status = models.CharField(
		max_length=1,
		choices=CHOICE_DICT["STATUS"])
	number_of_photos = models.IntegerField(
		blank=True,
		null=True)
	paper_margin = models.CharField(
		max_length=1,
		choices=CHOICE_DICT["PEPER_MARGIN"])
	paper_surface = models.CharField(
		max_length=1,
		choices=CHOICE_DICT["PEPER_SURFACE"])
	paper_size = models.CharField(
		max_length=3,
		choices=CHOICE_DICT["PEPER_SIZE_ALL"])
	paper_type = models.CharField(
		max_length=1,
		choices=CHOICE_DICT["PEPER_TYPE"])
	user = models.ForeignKey(UserProfile)


class Images_in_order(models.Model):
	image = models.ForeignKey(Images)
	order = models.ForeignKey(Log_order)
