# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''

from django import forms
from Photo2Print.settings import *
from Application.models import *
from django.contrib.admin.widgets import *
from suit.widgets import SuitDateWidget, SuitTimeWidget, SuitSplitDateTimeWidget
import datetime
class paper_choice(forms.Form):
    margin = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICE_DICT["PEPER_MARGIN"])
    surface = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICE_DICT["PEPER_SURFACE"])
    types = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICE_DICT["PEPER_TYPE"])
    price_all = forms.ChoiceField(widget=forms.Select, choices=CHOICE_DICT["PEPER_PRICE_ALL"])
    size_all = forms.ChoiceField(widget=forms.Select, choices=CHOICE_DICT["PEPER_SIZE_ALL"])
    size_fuji_matt = forms.ChoiceField(widget=forms.Select, choices=CHOICE_DICT["PEPER_SIZE_FUJI_MATT"])
    size_fuji_grossy = forms.ChoiceField(widget=forms.Select, choices=CHOICE_DICT["PEPER_SIZE_FUJI_GROSSY"])
    size_koduk_matt = forms.ChoiceField(widget=forms.Select, choices=CHOICE_DICT["PEPER_SIZE_KODUK_MATT"])
    size_koduk_grossy = forms.ChoiceField(widget=forms.Select, choices=CHOICE_DICT["PEPER_SIZE_KODUK_GROSSY"])
    size_super_matt = forms.ChoiceField(widget=forms.Select, choices=CHOICE_DICT["PEPER_SIZE_SUPER_MATT"])
    size_super_grossy = forms.ChoiceField(widget=forms.Select, choices=CHOICE_DICT["PEPER_SIZE_SUPER_GROSSY"])
    status = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICE_DICT["STATUS"])
    price_checked = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICE_DICT["PRICE_CHECKED"])
    order_confirmed = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICE_DICT["ORDER_CONFIRMED"])

class other_choice(forms.Form):
    permissionalbum = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICE_DICT["PERMISSIONALBUM"])
    permission = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICE_DICT["PERMISSION"])

class dateTimeForms(forms.Form):
    day_paid = forms.ChoiceField(label="day_paid", widget=forms.Select, choices=CHOICE_DICT["DAY"])
    month_paid = forms.ChoiceField(label="month_paid", widget=forms.Select, choices=CHOICE_DICT["MONTH"])
    year_paid = forms.ChoiceField(label="year_paid", widget=forms.Select, choices=CHOICE_DICT["YEAR"])
    hour_paid = forms.ChoiceField(label="hour_paid", widget=forms.Select, choices=CHOICE_DICT["HOUR"])
    minute_paid = forms.ChoiceField(label="minute_paid", widget=forms.Select, choices=CHOICE_DICT["MINUTE"])

class RegistrationForm(forms.Form):
    username = forms.CharField(label="",widget=forms.TextInput(attrs={'placeholder': 'Nom d\'utilisateur','class':'form-control input-perso'}),max_length=30,min_length=3,validators=[isValidUsername, validators.validate_slug])
    email = forms.EmailField(label="",widget=forms.EmailInput(attrs={'placeholder': 'Email','class':'form-control input-perso'}),max_length=100,error_messages={'invalid': ("Email invalide.")},validators=[isValidEmail])
    password1 = forms.CharField(label="",max_length=50,min_length=6,
                                widget=forms.PasswordInput(attrs={'placeholder': 'Mot de passe','class':'form-control input-perso'}))
    password2 = forms.CharField(label="",max_length=50,min_length=6,
                                widget=forms.PasswordInput(attrs={'placeholder': 'Confirmer mot de passe','class':'form-control input-perso'}))

    #recaptcha = ReCaptchaField()

    #Override clean method to check password match
    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password1 != password2:
            self._errors['password2'] = ErrorList([u"Le mot de passe ne correspond pas."])

        return self.cleaned_data

    #Override of save method for saving both User and Profile objects
    def save(self, datas):
        u = User.objects.create_user(datas['username'],
                                     datas['email'],
                                     datas['password1'])
        u.is_active = False
        u.save()
        profile=Profile()
        profile.user=u
        profile.activation_key=datas['activation_key']
        profile.key_expires=datetime.datetime.strftime(datetime.datetime.now() + datetime.timedelta(days=2), "%Y-%m-%d %H:%M:%S")
        profile.save()
        return u

    #Sending activation email ------>>>!! Warning : Domain name is hardcoded below !!<<<------
    #The email is written in a text file (it contains templatetags which are populated by the method below)
    def sendEmail(self, datas):
        link="http://yourdomain.com/activate/"+datas['activation_key']
        c=Context({'activation_link':link,'username':datas['username']})
        f = open(MEDIA_ROOT+datas['email_path'], 'r')
        t = Template(f.read())
        f.close()
        message=t.render(c)
        #print unicode(message).encode('utf8')
        send_mail(datas['email_subject'], message, 'yourdomain <no-reply@yourdomain.com>', [datas['email']], fail_silently=False)