# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''

from StringIO import StringIO
from zipfile import ZipFile
from django.core.servers.basehttp import FileWrapper
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
from Application.models import *
from django.template import RequestContext
from django.conf import settings
from Application.forms import *
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.shortcuts import render_to_response
from django.contrib import messages
import datetime
import sys
from django.utils.safestring import mark_safe
import dateutil.parser
from django.core.mail import send_mail
import hashlib
import random
reload(sys)
sys.setdefaultencoding('utf-8')

# Peper choice object
PaperChoice = paper_choice()
# Other choice object
other_choice = other_choice()
dateTimeForms = dateTimeForms()
##### Path function (Function helper) For get path of filename
def Path(instance, filename):
	return 'Account/%s/%s' % (instance.username, filename)

##########################
##### Authentication #####
def Page_GuestLogin(request):
	# This is contact page in status signout
	if request.user.is_authenticated():
		# return to Contact page in status signin
		return HttpResponseRedirect('/%s/ViewContactPage/' % request.user.username)
	# return to Contact page in status signout
	return render_to_response('ManageUser/Registration/LoginPage.html', RequestContext(request))

def Function_AllUserLogin(request):
	# Check if user is authenticated
	if request.user.is_authenticated():
		# Change to home page
		return HttpResponseRedirect('/%s/HomePage/' % request.user.username)
	# Check input text field Username and Password must not NULL
	if request.POST['username']\
	and request.POST['password']:
		username_in = request.POST['username']
		password_in = request.POST['password']
		# Authenticate User
		user = auth.authenticate(username=username_in, password=password_in)
		# If user is not none, Can use this Account to authenticate
		if user is not None:
			# Login user
			login(request, user)
			request.session['member_id'] = user.username
			# return to Home page
			return HttpResponseRedirect('/%s/HomePage/' % request.user.username)
		# if not have Username in database or wrong password
		# get error text
		errors = ("Your username and password didn't match. Please try again.")
		# return to login page with error message
		messages.error(request, errors)
		return HttpResponseRedirect('/LoginPage/')
		# return render_to_response(
		# 	'ManageUser/Registration/LoginPage.html',
		# 	{'errors': errors},
		# 	RequestContext(request))
	# If Username or password was NULL
	# get error text
	errors = ('Empty data or some data is NULL.')
	# return to login page with error message
	messages.error(request, errors)
	return HttpResponseRedirect('/LoginPage/')


def Function_AllUserLogout(request):
	# Try to logout
	try:
		logout(request)
	except KeyError:
		pass
	return HttpResponseRedirect('/HomePage/')

##########################
#####    Home Page   #####
def Page_HomeStatusSingIn(request, username):
	# This is home page in status signin
	# If user was not authenticated
	if not request.user.is_authenticated():
		# return to Home page in status signout
		return HttpResponseRedirect('/HomePage/')
	# return to Home page in status signin
	return render_to_response('General/HomePage.html', RequestContext(request))


def Page_HomeStatusSingOut(request):
	# This is home page in status signout
	# If user was authenticated
	if request.user.is_authenticated():
		# return to Home page in status signin
		return HttpResponseRedirect('/%s/HomePage/' % request.user.username)
	# return to Home page in status signout
	return render_to_response('General/HomePage.html', RequestContext(request))


def Page_HomeStatusSingOut_domainUrl(request):
	# This is home page in status signout
	# If user was authenticated
	if request.user.is_authenticated():
		# return to Home page in status signin
		return HttpResponseRedirect('/%s/HomePage/' % request.user.username)
	# return to Home page in status signout
	return HttpResponseRedirect('/HomePage/')

##########################
#####     Contact    #####
def Page_AllUserViewContact(request, username):
	# This is contact page in status signin
	# If user was not authenticated
	if not request.user.is_authenticated():
		# return to Contact page in status signout
		return HttpResponseRedirect('/ViewContactPage/')
	# return to Contact page in status signin
	return render_to_response('General/ContactPage.html', RequestContext(request))


def Page_GuestViewContact(request):
	# This is contact page in status signout
	if request.user.is_authenticated():
		# return to Contact page in status signin
		return HttpResponseRedirect('/%s/ViewContactPage/' % request.user.username)
	# return to Contact page in status signout
	return render_to_response('General/ContactPage.html', RequestContext(request))

##########################
#####   Detial User  #####
def Page_MemberViewDetailSelf(request, username):
	# if user was not authenticated
	if not request.user.is_authenticated():
		# return Home page
		return HttpResponseRedirect('/')
	# return detail user page
	return render_to_response('ManageUser/UserDetail/MemberDetail.html', RequestContext(request))


def Page_AllUserEditDetailUser(request, username, usernamepost):
	# This page is use to edit user profile
	# Check if user in not authenticated
	if not request.user.is_authenticated():
		# Return to Home page
		return HttpResponseRedirect('/')
	# If user is authenticate
	# Check username post was not NULL
	if not request.POST.get('username_requests', False)\
	or not request.POST.get('username_edit', False):

		# If Username post was NULL
		# Get error message
		errors = ('Empty data or some data is NULL.')
		messages.error(request, errors)
		# If user is not adminuser
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/SelfDetailPage/' % request.user.username)
		# return to edit usermember with error message
		return HttpResponseRedirect('/%s/AdminViewMemberListPage/' % request.user.username)
	if not (request.user.username == request.POST['username_edit'])\
	and not request.user.is_staff:
		errors = ("You don't have permission for edit other user.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/SelfDetailPage/' % request.user.username)
	# Get username
	username_requests_in = request.POST['username_requests']
	username_edit_in = request.POST['username_edit']
	# Check must have username in database
	if User.objects.filter(username=username_edit_in):
		# Get user object
		user_edit = User.objects.filter(username=username_edit_in)
		# Get userProfile object
		user_profile_edit = UserProfile.objects.filter(user=user_edit)
		# return edituser page with user object (User detail)
		return render_to_response('ManageUser/UserDetail/AllUserEditDetailUser.html',
			{'USER': user_profile_edit},
			RequestContext(request))
	# If not have user in database
	# return edituser page with error message
	# Get error message
	errors = ('Not found this username.')
	messages.error(request, errors)
	# If user is not adminuser
	if not request.user.is_staff:
		return HttpResponseRedirect('/%s/SelfDetailPage/' % request.user.username)
	# return to edit usermember with error message
	return HttpResponseRedirect('/%s/AdminViewMemberListPage/' % request.user.username)

def Fucntion_AllUserEditDetailUser(request):
	#Check data in Profile text field is not NULL
	if not request.POST['oldusername']:
		errors = ('Unexpect Error : Old username in empty.')
		messages.error(request, errors)
		# If user is not adminuser
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/SelfDetailPage/' % request.user.username)
		# return to edit usermember with error message
		return HttpResponseRedirect('/%s/AdminViewMemberListPage/' % request.user.username)
	username_old_in = request.POST['oldusername']
	# Get object user model by old username
	user_object = User.objects.get(username=username_old_in)
	user_profile_object = UserProfile.objects.filter(user=user_object)
	if not request.POST['firstname']\
	or not request.POST['lastname']\
	or not request.POST['permission']\
	or not request.POST['username']\
	or not request.POST['e_mail']:
		# If data text field was NULL
		# Get message error
		errors = ('Empty data or some data is NULL.')
		# If user is not adminuser
		if not request.user.is_staff:
			return render_to_response('ManageUser/UserDetail/AllUserEditDetailUser.html',
				{	'errors': errors,
					'USER': user_profile_object},
				RequestContext(request))
		# return to edit usermember with error message
		return render_to_response('ManageUser/UserDetail/AllUserEditDetailUser.html',
			{	'errors': errors,
				'USER': user_profile_object},
			RequestContext(request))

	# Get Profile data change in text field
	username_new_in = request.POST['username']
	firstName_in = request.POST['firstname']
	lastname_in = request.POST['lastname']
	e_mail_in = request.POST['e_mail']
	permission_in = request.POST['permission']
	# Set permission
	if str(permission_in) == "A":
		user_object.is_superuser = True
		user_object.is_staff = True
	else:
		user_object.is_superuser = False
		user_object.is_staff = False
	# Set New e-mail
	user_object.email = e_mail_in
	# Set New username
	user_object.username = username_new_in
	# If set new password
	if request.POST['password_c']\
	and request.POST['password']:
		password_in = request.POST['password']
		password_C = request.POST['password_c']
		#If confirm password is match
		if not password_C == password_in:
			#IF COMFIRM PASSWORD IS FALSE
			errors = ("Your password and comfirm password didn't match. Please try again.")
			# If user is not adminuser
			if not request.user.is_staff:
				return render_to_response('ManageUser/UserDetail/AllUserEditDetailUser.html',
				{	'errors': errors,
					'USER': user_profile_object},
				RequestContext(request))
			# return to edit usermember with error message
			return render_to_response('ManageUser/UserDetail/AllUserEditDetailUser.html',
				{	'errors': errors,
					'USER': user_profile_object},
				RequestContext(request))
			# Set New password
		user_object.set_password(password_in)
	try:
		# Try rename directory user to new username
		os.rename(
			os.path.join('media', 'Account', username_old_in),
			os.path.join('media', 'Account', username_new_in))
	except OSError:
		# Get error text
		errors = ("Can't to rename directory user.")
		# return edit user page with message error
		# If user is not adminuser
		if not request.user.is_staff:
			return render_to_response('ManageUser/UserDetail/AllUserEditDetailUser.html',
				{	'errors': errors,
					'USER': user_profile_object},
				RequestContext(request))
		# return to edit usermember with error message
		return render_to_response('ManageUser/UserDetail/AllUserEditDetailUser.html',
			{	'errors': errors,
				'USER': user_profile_object},
			RequestContext(request))
	# Set New firstname
	user_object.first_name = firstName_in
	# Set New Lastname
	user_object.last_name = lastname_in
	# save user object
	user_object.save()
	# Get UserProfile object
	user_profile_object = UserProfile.objects.get(user=user_object)
	# Set quota
	user_profile_object.quota = 500
	# Set permission
	user_profile_object.permission = permission_in
	# Save Userprofile object
	user_profile_object.save()
	if not request.user.is_staff:
		return HttpResponseRedirect('/%s/SelfDetailPage/' % username_new_in)
	return HttpResponseRedirect('/%s/AdminViewMemberListPage/' % request.user.username)

##########################
#####  Registration  #####
def Page_GuestRegister(request):
	# This is register page
	# If user have authenticated
	if request.user.is_authenticated():
		# return to home page in status signin
		return HttpResponseRedirect('/%s/HomePage/' % request.user.username)
	# return to register page
	return render_to_response('ManageUser/Registration/RegisterPage.html', RequestContext(request))


def Function_GuestRegister(request):
	# This function is behavior of register
	# If user have authenticated
	if request.user.is_authenticated():
		# return to home page in status signin
		return HttpResponseRedirect('/%s/' % request.user.username)
	# Check if Profile text field was not NULL
	if not request.POST['firstname']\
	or not request.POST['lastname']\
	or not request.POST['permission']\
	or not request.POST['username']\
	or not request.POST['e_mail']\
	or not request.POST['password_c']\
	or not request.POST['password']:
		# if Data text field is NULL
		# Get error message
		errors = ('Empty data or some data is NULL.')
		# return to register page with error message
		messages.error(request, errors)
		return HttpResponseRedirect('/RegisterPage/')
		# return render_to_response(
		# 	'ManageUser/Registration/RegisterPage.html',
		# 	{'errors': errors},
		# 	RequestContext(request))
	# Get Profile Data from text field
	firstName_in = request.POST['firstname']
	lastname_in = request.POST['lastname']
	username_in = request.POST['username']
	e_mail_in = request.POST['e_mail']
	password_in = request.POST['password']
	password_C = request.POST['password_c']
	permission_in = request.POST['permission']

	#If username in text field is same username in database
	if User.objects.filter(username=username_in):
		# Get message error
		errors = ("This username is can't be use")
		# return to register page with error message
		messages.error(request, errors)
		return HttpResponseRedirect('/RegisterPage/')
		# return render_to_response(
		# 	'ManageUser/Registration/RegisterPage.html',
		# 	{'errors': errors},
		# 	RequestContext(request))
	#If username ready to use
	#If confirm password is match
	if not password_C == password_in:
		#If confirm password is false
		# Get error message
		errors = ("Your password and comfirm password didn't match. Please try again.")
		# return to register page with error message
		messages.error(request, errors)
		return HttpResponseRedirect('/RegisterPage/')
		# return render_to_response(
		# 	'ManageUser/Registration/RegisterPage.html',
		# 	{'errors': errors},
		# 	RequestContext(request))

	# Try make Directory name same username in media/Account/ path
	try:
	   # with open("Debug","a") as debug:
	   #     debug.write("{}".format(os.path.join('media', 'Account', username_in)))
		os.mkdir(os.path.join('media', 'Account', username_in))
	except OSError:
		# Get error message
		errors = ("Can't to create directory user.")
		# return to registe page with error message
		messages.error(request, errors)
		return HttpResponseRedirect('/RegisterPage/')
		# return render_to_response(
		# 	'ManageUser/Registration/RegisterPage.html',
		# 	{'errors': errors},
		# 	RequestContext(request))

	# Begin create username
	user_object = User.objects.create_user(username=username_in, password=password_in)
	# Set permission
	if permission_in == "A":
		user_object.is_superuser = True
		user_object.is_staff = True
	else:
		user_object.is_superuser = False
		user_object.is_staff = False
	# Set e-mail
	user_object.email = e_mail_in
	# Set Firstname
	user_object.first_name = firstName_in
	# Set Lastname
	user_object.last_name = lastname_in
	# Set User active
	user_object.is_active = False
	# Save create user
	user_object.save()
	# Begin Create user profile (one to one User model)
	user_profile_object = UserProfile.objects.get(user=user_object)
	# Set quota
	user_profile_object.quota = 500
	# Set permission
	user_profile_object.permission = permission_in
	# Save Create User profile
	user_profile_object.save()
	# Begin Create activation profile (one to one User model)
	user_activate_object = UserActivate.objects.get(user=user_object)
	# Set activate key
	salt = hashlib.sha1(str(random.random()).encode('utf-8')).hexdigest()[:5]
	user_activate_object.activation_key = hashlib.sha1(
		str(salt + username_in).encode('utf8')).hexdigest(
	# Set key expires
	user_activate_object.key_expires = datetime.datetime.strftime(
		datetime.datetime.now() + datetime.timedelta(days=2), "%Y-%m-%d %H:%M:%S")
	# Save Create User activate
	user_activate_object.save()
	# Test send email
	send_mail(
        'Subject here',
        'Here is the message.',
        'from@example.com',
        ['smallman.developer@gmail.com'],
        fail_silently=False,
    )
	# Get success message
	success = ('Register is success.')
	# Return to login page with success message
	messages.success(request, success)
	return HttpResponseRedirect('/LoginPage/')

def Function_AdminDeleteUser(request):
	# Check if user is not staff then return to main page
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	request.user.username = request.user.username
	if not request.POST['username']:
		if not request.user.is_staff:
			return HttpResponseRedirect('/')
		errors = ("Username POST was empty.")
		return HttpResponseRedirect('/%s/AdminViewMemberListPage/' % request.user.username)
	username_in = request.POST['username']
	user_object = User.objects.get(username=username_in)
	user_profile_object = UserProfile.objects.get(user=user_object)
	album_object = Album.objects.filter(user=user_profile_object)
	for album_index in range(len(album_object)):
		image_object = Images.objects.filter(
			album=Album.objects.get(
				user=user_object,
				albumname=album_object[album_index].albumname))
		for image_index in range(len(image_object)):
			imgo = image_object[image_index].imagename
			os.remove(
				os.path.join(
					'media', 'Account',
					username_in,
					album_object[album_index].albumname,
					imgo.encode("utf-8")))
			os.remove(
				os.path.join(
					'media', 'Account',
					username_in,
					album_object[album_index].albumname,
					'Thumpnail',
					imgo.encode("utf-8")))
		os.rmdir(
			os.path.join(
				'media', 'Account',
				username_in,
				album_object[album_index].albumname,
				'Thumpnail'))
		os.rmdir(
			os.path.join(
				'media', 'Account',
				username_in,
				album_object[album_index].albumname))
		Images.objects.filter(
			album=album_object[album_index]).delete()
		album = Album.objects.filter(
			user=user_profile_object,
			albumname=album_object[album_index].albumname).delete()
	os.rmdir(os.path.join('media', 'Account', username_in))
	user_profile_object.delete()
	user_object.delete()
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	return HttpResponseRedirect('/%s/AdminViewMemberListPage/' % request.user.username)

##########################
#####  Albums Event  #####
def Page_MemberViewAlbums(request, username):
	# Check if user is not authenticate then return to main page
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=request.user.username)
	album_object = Album.objects.filter(user=user_object)
	paginator = Paginator(album_object, 9)
	# Show 25 contacts per page
	page = request.GET.get('page')
	try:
		albums_all = paginator.page(page)
	except PageNotAnInteger:
		albums_all = paginator.page(1)
	except EmptyPage:
		albums_all = paginator.page(paginator.num_pages)
	return render_to_response(
		'Gallery/Member/MemberViewAlbums.html',
		{'Albums': albums_all},
		RequestContext(request))

def Page_MemberAddAlbum(request, username):
	# Check if user is not authenticate then return to main page
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	return render_to_response(
		'Gallery/Member/MemberAddAlbum.html',
		{'permission': other_choice},
		RequestContext(request))

def Function_MemberAddAlbum(request):
	# Check if user is not authenticate then return to main page
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.POST.get('permissionalbum', False)\
	or not request.POST['comment'] \
	or not request.POST['username']\
	or not request.POST['album']:
		# the data get incomplete
		errors = ('Empty Data.')
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AddAlbumPage/' % request.user.username)

	permission_in = request.POST['permissionalbum']
	username_in = request.POST['username']
	albumname_in = request.POST['album']
	comment_in = request.POST['comment']
	if os.path.exists(
		os.path.join('media', 'Account', username_in, albumname_in)):
		errors = ('Albumname exists.')
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AddAlbumPage/' % request.user.username)
	user_object = User.objects.get(username=username_in)
	user_profile_object = UserProfile.objects.get(user=user_object)
	album_object = Album.objects.create(
		albumname=albumname_in,
		albumcover="/media/images/picnull.jpeg",
		comment=comment_in,
		permissionalbum=permission_in,
		user=user_profile_object)
	album_object.save()
	try:
		os.mkdir(os.path.join('media', 'Account', username_in, albumname_in))
		os.mkdir(os.path.join('media', 'Account', username_in, albumname_in, 'Thumpnail'))
	except OSError:
		errors = ('Cannot create directory.')
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AddAlbumPage/' % request.user.username)
	return HttpResponseRedirect(
		'/%s/GalleryPage/AlbumName/%s/UploadPicturePage/' % (username_in, albumname_in))


def Function_MemberDeleteAlbum(request):
	# Check if user is not authenticate then return to main page
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.POST['albumname']:
		errors = ("Albumname POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/GalleryPage/' % (request.user.username))
	albumname_in = request.POST['albumname']
	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	album_object = Album.objects.filter(user=user_profile_object, albumname=albumname_in)
	image_object = Images.objects.filter(album=album_object)
	for index in range(len(image_object)):
		imgo = (image_object[index].imagename)
		os.remove(
			os.path.join(
				'media', 'Account',
				(request.user.username),
				albumname_in,
				(imgo.encode("utf-8"))))
		os.remove(
			os.path.join(
				'media', 'Account',
				(request.user.username),
				albumname_in,
				'Thumpnail',
				(imgo.encode("utf-8"))))
	os.rmdir(
		os.path.join(
			'media', 'Account',
			(request.user.username),
			albumname_in,
			'Thumpnail'))
	os.rmdir(
		os.path.join(
			'media', 'Account',
			(request.user.username),
			albumname_in))
	image_object.delete()
	album_object.delete()
	return HttpResponseRedirect('/%s/GalleryPage/' % (request.user.username))

##########################
#####  Picture Event #####
def Page_MemberViewPictures(request, username, albumname):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumname
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
	image_object = Images.objects.filter(album=album_object)
	comment_in = (album_object.comment)
	paginator = Paginator(image_object, 9)
	# Show 25 contacts per page
	page = request.GET.get('page')
	try:
		image_all = paginator.page(page)
	except PageNotAnInteger:
		image_all = paginator.page(1)
	except EmptyPage:
		image_all = paginator.page(paginator.num_pages)
	return render_to_response(
		'Gallery/Member/MemberViewPicture.html',
		{
			'image_all': image_all,
			'albumname': albumname_in,
			'comment': comment_in
		},
		RequestContext(request))


def Page_MemberViewFullPictures(request, username, albumname, imagename):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumname
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
	imagename_in = imagename
	try:
		image_object = Images.objects.filter(album=album_object, imagename=imagename_in)
	except Images.DoesNotExist:
		errors = ("Picture name does not exist.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AlbumName/%s/' % (request.user.username,albumname_in))
	return render_to_response(
		'Gallery/Member/MemberViewFullPicture.html',
		{
			'image': image_object,
			'albumname': albumname_in
		},
		RequestContext(request))

def Page_MemberUploadPicture(request, username, albumname):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumname
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
	album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in )
	return render_to_response(
		'Gallery/Member/MemberUploadPicture.html',
		{
			'Albums': album_object,
			'albumname': albumname_in
		},
		RequestContext(request))

def Function_MemberUploadPicture(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.POST['albumname']:
		errors = ("Albumname POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/GalleryPage/' % (request.user.username))
	if not request.FILES.get('imagefile', False):
		albumname_in = request.POST['albumname']
		errors = ('Empty data or some data is NULL.')
		messages.error(request, errors)
		return HttpResponseRedirect(
			'/%s/GalleryPage/AlbumName/%s/UploadPicturePage/' % (request.user.username,albumname_in))
	albumname_in = request.POST['albumname']
	imagename_in = request.FILES['imagefile']
	if Images.objects.filter(
			imagepath='Account/' + \
			request.user.username + '/' + \
			albumname_in + '/' + \
			imagename_in.name):
		errors = ('Image name this Repeatedly.')
		messages.error(request, errors)
		return HttpResponseRedirect(
			'/%s/GalleryPage/AlbumName/%s/UploadPicturePage/' % (request.user.username,albumname_in))
	try:
		from PIL import Image
	except ImportError:
		import Image
	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	Path_Picture_Thumpnail = \
		os.path.join("media", "Account", request.user.username, albumname_in, "Thumpnail")
	Path_Picture_full =\
		os.path.join("media", "Account", request.user.username, albumname_in)
	album_object.albumcover = (os.path.join( os.sep, Path_Picture_Thumpnail, str(imagename_in)))
	image_object = Images.objects.create(
		album=album_object,
		imagepath=imagename_in,
		imagename=imagename_in.name)
	image_pil_object = Image.open(os.path.join( Path_Picture_full, str(imagename_in)))
	(width, height) = image_pil_object.size
	size = image_pil_object.size
	image_pil_object.thumbnail((260, 190), Image.ANTIALIAS)
	image_pil_object.save(
		os.path.join(
			'media', 'Account', request.user.username,
			albumname_in, "Thumpnail", imagename_in.name),
		quality=75)
	image_object.save()
	image_object = Images.objects.get(album=album_object, imagename=imagename_in.name)
	image_object.width = int(width)
	image_object.height = int(height)
	image_object.save()
	album_object.save()
	success = ('Complete.')
	messages.success(request, success)
	return HttpResponseRedirect(
		'/%s/GalleryPage/AlbumName/%s/UploadPicturePage/' % (request.user.username,albumname_in))


def Fucntion_MemberDeletePicture(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.POST['albumname']:
		errors = ("Albumname POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/GalleryPage/' % (request.user.username))
	albumname_in = request.POST['albumname']
	if not request.POST['imagename']:
		errors = ("Picture name POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/GalleryPage/AlbumName/%s/' % (request.user.username,albumname_in))
	imagename_in = request.POST['imagename']
	user_object = User.objects.get(username=(request.user.username))
	user_profile_object = UserProfile.objects.get(user=user_object)
	album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	image_object = Images.objects.get(album=album_object, imagename=imagename_in)
	os.remove(
		os.path.join(
			'media','Account',
			(request.user.username),
			albumname_in,
			str(imagename_in)))
	os.remove(
		os.path.join(
			'media','Account',
			(request.user.username),
			albumname_in,
			'Thumpnail',
			str(imagename_in)))
	Images.objects.filter(
		album=album_object,
		imagename=imagename_in).delete()
	pathPictureCandidate = os.path.join(
		os.sep, 'media','Account',
		request.user.username,
		album_object.albumname,
		"Thumpnail",
		imagename_in)
	if (album_object.albumcover == pathPictureCandidate):
		images_new = Images.objects.filter(album=album_object).order_by('-id')[:1]
		imagename_new = None
		if (len(images_new) == 0):
			album_object.albumcover = "/media/images/picnull.jpeg"
			album_object.save()
		else:
			for image in images_new:
				album_object.albumcover = os.path.join(
					os.sep,	'media','Account',
					request.user.username,
					album_object.albumname,
					"Thumpnail",
					image.imagename)
				album_object.save()
	return HttpResponseRedirect('/%s/GalleryPage/AlbumName/%s/' % ((request.user.username), (album_object.albumname)))

##########################
#####  Member Event  #####
def Page_AdminViewMemberList(request, username):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	user_object_list = UserProfile.objects.all()
	return render_to_response('ManageUser/UserDetail/AdminViewMemberList.html',
		{'USER': user_object_list},
		RequestContext(request))

##########################
#####  Order Event   #####
def Function_AllUserDownloadFile(request):
	if not request.POST['username']\
	or not request.POST['albumname']\
	or not request.POST['date_time']\
	or not request.POST['imagename']:
		return HttpResponseRedirect('/')
	username_in = request.POST['username']
	albumname_in = request.POST['albumname']
	imagename_in = request.POST['imagename']
	file_imagename = os.path.join(
		settings.MEDIA_ROOT, "Account", username_in, albumname_in, imagename_in)
	wrapper = FileWrapper(open(file_imagename, 'rb'))
	response = HttpResponse(wrapper, content_type='application/force-download')
	response['Content-Disposition'] = "attachment; filename=%s_%s" % (username_in, imagename_in)
	return response


def Function_AllUserDownloadZipFile(request):
	if not request.POST['username']\
	or not request.POST['datetime']:
		return HttpResponseRedirect('/')
	username_in = request.POST['username']
	datetime_in = request.POST['datetime']
	user_object = User.objects.get(username=username_in)
	user_profile_object = UserProfile.objects.get(user=user_object)
	order_object = Log_order.objects.get(user=user_profile_object, date_time=datetime_in)
	order_object.status = 'P'
	order_object.save()
	in_memory = StringIO()
	datetime_object = dateutil.parser.parse(datetime_in)
	compress = ZipFile(in_memory, "w")
	image_in_order_object_list = Images_in_order.objects.filter(order=order_object)
	for image_in_order_object in image_in_order_object_list:
		imagePath = unicode(MEDIA_ROOT) + unicode(image_in_order_object.image.imagepath)
		imageName = image_in_order_object.image.imagename
		compress.write(imagePath, imageName)
	for file in compress.filelist:
		file.create_system = 0
	compress.close()
	response = HttpResponse(in_memory.getvalue(), content_type="application/zip")
	response['Content-Disposition'] = "attachment; filename=%s_%s.zip" % (username_in, datetime_object.strftime("%Y-%m-%dT%H:%M:%S"))
	return response

def Page_AdminEditOrderStatus(request, username, usernamepost, date_time):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.POST['datetime_order']\
	or not request.POST['username_order']:
		errors = ('Empty Data.')
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/OrderPage/' % (request.user.username))
		return HttpResponseRedirect(
			'/%s/AdminViewMemberOrder/%s/' % (request.user.username, usernamepost))
	username_order_in = request.POST['username_order']
	datetime_order_in = request.POST['datetime_order']
	if not request.user.is_staff:
		return HttpResponseRedirect('/%s/OrderPage/' % (request.user.username))
	return render_to_response(
		'Order/Admin/AdminEditOrderStatus.html',
		{
			'username_order': username_order_in,
			'datetime_order': datetime_order_in,
			'paper': PaperChoice
		},
		RequestContext(request))

def Function_AdminEditOrderStatus(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.POST['datetime_order']\
	or not request.POST['username_order']:
		errors = ('Empty Data.')
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/OrderPage/' % (request.user.username))
		return HttpResponseRedirect(
			'/%s/AdminViewAllMemberOrder/' % (request.user.username))
	username_order_in = request.POST['username_order']
	datetime_order_in = request.POST['datetime_order']
	if not request.POST.get('status'):
		errors = "Must select some choice."
		return render_to_response(
			'Order/Admin/AdminEditOrderStatus.html',
			{
				'username_order': username_order_in,
				'datetime_order': datetime_order_in,
				'paper': PaperChoice,
				'errors' : errors
			},RequestContext(request))
	status_in = request.POST['status']
	user_object = User.objects.get(username=username_order_in)
	user_profile_object = UserProfile.objects.get(user=user_object)
	order_object = Log_order.objects.get(user=user_profile_object, date_time=datetime_order_in)
	order_object.status = status_in
	order_object.save()
	if not request.user.is_staff:
		return HttpResponseRedirect('/%s/OrderPage/' % (request.user.username))
	return HttpResponseRedirect(
		'/%s/AdminViewMemberOrder/%s/' % (request.user.username, username_order_in))

#####  DELETE ORDER  #####
def Function_AdminDeleteOrder(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.POST['imagename']\
	or not request.POST['albumname']\
	or not request.POST['date_time']\
	or not request.POST['username']:
		errors = ('Empty Data.')
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/OrderPage/' % (request.user.username))
		return HttpResponseRedirect(
			'/%s/AdminViewMemberOrder/%s/' % (request.user.username, username_in))
	imagename_in = request.POST['imagename']
	albumname_in = request.POST['albumname']
	username_in = request.POST['username']
	datetime_in = request.POST['date_time']
	user_object = User.objects.get(username=username_in)
	user_profile_object = UserProfile.objects.get(user=user_object)
	album_object = Album.objects.filter(
		user=user_profile_object,
		albumname=albumname_in)
	image_object = Images.objects.filter(
		album=album_object,
		imagename=imagename_in)
	order = Log_order.objects.filter(
		user=user_profile_object,
		image=image_object,
		date_time=datetime_in).delete()
	if not request.user.is_staff:
		return HttpResponseRedirect(
			'/%s/OrderPage/' % (request.user.username))
	return HttpResponseRedirect(
		'/%s/AdminViewMemberOrder/%s/' % (request.user.username, username_in))

#####  ORDER ALBUM   #####
def Page_MemberOrderAlbumType(request, username, usernamepost, albumnamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumnamepost
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)
	return render_to_response(
		'Order/Member/MemberSelectPaperTypeInOrderAlbum.html',
		{
			'username': username,
			'usernamepost' : usernamepost,
			'albumname': albumname_in,
			'paper': PaperChoice
		}, RequestContext(request))

def Page_MemberOrderAlbumSurface(request, username, usernamepost, albumnamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumnamepost
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)
	if not request.POST.get('types'):
		errors = "Must select some choice."
		return render_to_response(
		'Order/Member/MemberSelectPaperTypeInOrderAlbum.html',
		{
			'username': request.user.username,
			'usernamepost' : usernamepost,
			'albumname': albumname_in,
			'paper': PaperChoice,
			'errors': errors
		},RequestContext(request))
	paper_type_in = request.POST['types']
	return render_to_response(
		'Order/Member/MemberSelectPaperSurfaceInOrderAlbum.html',
		{
			'username': request.user.username,
			'usernamepost' : usernamepost,
			'albumname': albumname_in,
			'paper': PaperChoice,
			'types': paper_type_in
		}, RequestContext(request))

def Page_MemberOrderAlbumSizeAndMargin(request, username, usernamepost, albumnamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumnamepost
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)
	if not request.POST.get('types', False):
		errors = ("Some data POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username,albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	paper_type_in = request.POST['types']
	if not 'surface' in request.POST:
		errors = "Must select some choice."
		return render_to_response(
		'Order/Member/MemberSelectPaperSurfaceInOrderAlbum.html',
		{
			'username': request.user.username,
			'usernamepost' : usernamepost,
			'albumname': albumname_in,
			'paper': PaperChoice,
			'types': paper_type_in,
			'errors': errors
		},RequestContext(request))
	paper_surface_in = request.POST['surface']
	return render_to_response(
		'Order/Member/MemberSelectPaperSizeAndMarginInOrderAlbum.html',
		{
			'username': request.user.username,
			'usernamepost' : usernamepost,
			'albumname': albumname_in,
			'paper': PaperChoice,
			'types': paper_type_in,
			'surface': paper_surface_in
		}, RequestContext(request))

def Page_MemberOrderAlbumCheck(request, username, usernamepost, albumnamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not 'albumname' in request.POST:
		errors = ("Albumname POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = request.POST['albumname']
	if not 'surface' in request.POST\
	and not request.POST.get('types', False):
		errors = ("Some data POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	paper_surface_in = request.POST['surface']
	paper_type_in = request.POST['types']
	paper_size_in = None
	if not 'margin' in request.POST:
		errors = "Must select some choice."
		return render_to_response(
			'Order/Member/MemberSelectPaperSizeAndMarginInOrderAlbum.html',
			{
				'username': request.user.username,
				'usernamepost': usernamepost,
				'albumname': albumnamepost,
				'paper': PaperChoice,
				'types': paper_type_in,
				'surface': paper_surface_in,
				'errors': errors
			},RequestContext(request))
	paper_margin_in = request.POST['margin']
	if paper_type_in == 'S' and paper_surface_in == 'G':
		paper_size_in = request.POST['size_super_grossy']
	elif paper_type_in == 'S' and paper_surface_in == 'M':
		paper_size_in = request.POST['size_super_matt']
	elif paper_type_in == 'F' and paper_surface_in == 'G':
		paper_size_in = request.POST['size_fuji_grossy']
	elif paper_type_in == 'F' and paper_surface_in == 'M':
		paper_size_in = request.POST['size_fuji_matt']
	elif paper_type_in == 'K' and paper_surface_in == 'G':
		paper_size_in = request.POST['size_koduk_grossy']
	elif paper_type_in == 'K' and paper_surface_in == 'M':
		paper_size_in = request.POST['size_koduk_matt']
	else:
		errors = ("Unexpect error : data choice invalid.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/AlbumName/%s/' % (
				request.user.username,albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)
	imageInAlbum_object = Images.objects.filter(album=album_object)
	order_object = Log_order.objects.create(
		user=user_profile_object,
		status='W',
		paper_margin=paper_margin_in,
		paper_surface=paper_surface_in,
		paper_size=paper_size_in,
		paper_type=paper_type_in,
		payment_is_checked=False,
		payment_is_confirm=False,
		price_perpic=paper_size_in)
	index_photo = 0
	for image in imageInAlbum_object:
		index_photo = index_photo + 1
	order_object.number_of_photos = index_photo
	order_object.price_total = int(index_photo) * int(order_object.get_price_perpic_display())
	order_object.save()
	tmpOrder = order_object
	order_object.delete()
	return render_to_response(
		'Order/Member/MemberCheckOrderPreSubmit.html',
		{
			'username': request.user.username,
			'usernamepost': usernamepost,
			'albumname': albumname_in,
			'paper': PaperChoice,
			'types': paper_type_in,
			'size': paper_size_in,
			'margin': paper_margin_in,
			'surface': paper_surface_in,
			'Order': tmpOrder
		}, RequestContext(request))


def Function_MemberOrderAlbum(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	usernamepost = request.POST['usernamepost']
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	if not request.POST['albumname']:
		errors = ("Albumname POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = request.POST['albumname']
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)

	if not request.POST['username']\
	and not request.POST['margin']\
	and not request.POST['surface']\
	and not request.POST['size']\
	and not request.POST.get('types', False):
		errors = ("Some data POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/AlbumName/%s/' % (
				request.user.username,albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	status_in = 'W'
	paper_margin_in = request.POST['margin']
	paper_surface_in = request.POST['surface']
	paper_size_in = request.POST['size']
	paper_type_in = request.POST['types']
	imageInAlbum_object = Images.objects.filter(album=album_object)
	order_object = Log_order.objects.create(
		user=user_profile_object,
		status='W',
		paper_margin=paper_margin_in,
		paper_surface=paper_surface_in,
		paper_size=paper_size_in,
		paper_type=paper_type_in,
		payment_is_checked=False,
		payment_is_confirm=False,
		price_perpic=paper_size_in)
	index_photo = 0
	for image in imageInAlbum_object:
		index_photo = index_photo + 1
		Images_in_order.objects.create(order=order_object, image=image)
		order_object.number_of_photos = index_photo
		order_object.price_total = int(index_photo) * int(order_object.get_price_perpic_display())
		order_object.save()
	if not request.user.is_staff:
		return HttpResponseRedirect(
			'/%s/OrderPage/' % (request.user.username))
	return HttpResponseRedirect('/%s/AdminViewMemberOrder/%s/' % (
		request.user.username, usernamepost))

#####   ORDER IMAGE  #####
def Page_MemberOrderImageType(request, username, usernamepost, albumnamepost, picturenamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumnamepost
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)
	picturename_in = picturenamepost
	try:
		image_object = Images.objects.get(album=album_object, imagename=picturename_in)
	except Images.DoesNotExist:
		errors = ("Picture name does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))
	picturename_in = (image_object.imagename)
	return render_to_response(
		'Order/Member/MemberSelectPaperTypeInOrderPicture.html',
		{
			'username': request.user.username,
			'usernamepost': usernamepost,
			'albumname': albumname_in,
			'picturename' : picturename_in,
			'paper': PaperChoice
		}, RequestContext(request))


def Page_MemberOrderImageSurface(request, username, usernamepost, albumnamepost, picturenamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumnamepost
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)
	picturename_in = picturenamepost
	try:
		image_object = Images.objects.get(album=album_object, imagename=picturename_in)
	except Images.DoesNotExist:
		errors = ("Picture name does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))
	picturename_in = (image_object.imagename)
	if not request.POST.get('types',False):
		errors = "Must select some choice."
		return render_to_response('Order/Member/MemberSelectPaperTypeInOrderPicture.html',
		{
			'username': request.user.username,
			'usernamepost': usernamepost,
			'albumname': albumname_in,
			'picturename': picturename_in,
			'paper': PaperChoice,
			'errors': errors
		},RequestContext(request))
	paper_type_in = request.POST['types']
	return render_to_response(
		'Order/Member/MemberSelectPaperSurfaceInOrderPicture.html',
		{
			'username': request.user.username,
			'usernamepost': usernamepost,
			'albumname': albumname_in,
			'picturename': picturename_in,
			'paper': PaperChoice,
			'types': paper_type_in
		}, RequestContext(request))

def Page_MemberOrderImageSizeAndMargin(request, username, usernamepost, albumnamepost, picturenamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumnamepost
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)
	picturename_in = picturenamepost
	try:
		image_object = Images.objects.get(album=album_object, imagename=picturename_in)
	except Images.DoesNotExist:
		errors = ("Picture name does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))
	picturename_in = (image_object.imagename)
	if not request.POST.get('types', False):
		errors = ("Some data POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))
	paper_type_in = request.POST['types']
	if not 'surface' in request.POST:
		errors = "Must select some choice."
		return render_to_response(
		'Order/Member/MemberSelectPaperSurfaceInOrderPicture.html',
		{
			'username': request.user.username,
			'usernamepost': usernamepost,
			'albumname': albumname_in,
			'picturename': picturename_in,
			'paper': PaperChoice,
			'types': paper_type_in,
			'errors': errors
		},RequestContext(request))
	paper_surface_in = request.POST['surface']
	return render_to_response(
		'Order/Member/MemberSelectPaperSizeAndMarginInOrderPicture.html',
		{
			'username': request.user.username,
			'usernamepost': usernamepost,
			'albumname': albumname_in,
			'picturename': picturename_in,
			'paper': PaperChoice,
			'types': paper_type_in,
			'surface': paper_surface_in
		}, RequestContext(request))

def Page_MemberOrderImageCheck(request, username, usernamepost, albumnamepost, picturenamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	albumname_in = albumnamepost
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)
	picturename_in = picturenamepost
	try:
		image_object = Images.objects.get(album=album_object, imagename=picturename_in)
	except Images.DoesNotExist:
		errors = ("Picture name does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))
	picturename_in = (image_object.imagename)
	if not 'surface' in request.POST\
	or not request.POST.get('types', False):
		errors = ("Some data POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))
	paper_surface_in = request.POST['surface']
	paper_type_in = request.POST['types']
	paper_size_in = "NULL"
	if not 'margin' in request.POST:
		errors = "Must select some choice."
		return render_to_response(
			'Order/Member/MemberSelectPaperSizeAndMarginInOrderPicture.html',
			{
				'username': request.user.username,
				'usernamepost': usernamepost,
				'albumname': albumname_in,
				'picturename': picturename_in,
				'paper': PaperChoice,
				'types': paper_type_in,
				'size': paper_size_in,
				'surface': paper_surface_in,
				'errors': errors
			},RequestContext(request))

	paper_margin_in = request.POST['margin']
	if paper_type_in == 'S' and paper_surface_in == 'G':
		paper_size_in = request.POST['size_super_grossy']
	elif paper_type_in == 'S' and paper_surface_in == 'M':
		paper_size_in = request.POST['size_super_matt']
	elif paper_type_in == 'F' and paper_surface_in == 'G':
		paper_size_in = request.POST['size_fuji_grossy']
	elif paper_type_in == 'F' and paper_surface_in == 'M':
		paper_size_in = request.POST['size_fuji_matt']
	elif paper_type_in == 'K' and paper_surface_in == 'G':
		paper_size_in = request.POST['size_koduk_grossy']
	elif paper_type_in == 'K' and paper_surface_in == 'M':
		paper_size_in = request.POST['size_koduk_matt']
	else:
		errors = ("Unexpect error : data choice invalid.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))
	order_object = Log_order.objects.create(
		user=user_profile_object,
		status='W',
		paper_margin=paper_margin_in,
		paper_surface=paper_surface_in,
		paper_size=paper_size_in,
		paper_type=paper_type_in,
		payment_is_checked=False,
		payment_is_confirm=False,
		price_perpic=paper_size_in)
	order_object.number_of_photos = 1
	order_object.price_total = int(order_object.get_price_perpic_display())
	order_object.save()
	tmpOrder = order_object
	order_object.delete()
	return render_to_response(
		'Order/Member/MemberCheckOrderPreSubmit.html',
		{
			'username': usernamepost,
			'usernamepost': usernamepost,
			'albumname': albumname_in,
			'picturename': picturename_in,
			'paper': PaperChoice,
			'types': paper_type_in,
			'size': paper_size_in,
			'margin': paper_margin_in,
			'surface': paper_surface_in,
			'Order': tmpOrder
		}, RequestContext(request))

def Function_MemberOrderImage(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	usernamepost = request.POST['usernamepost']
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	if not request.POST['albumname']:
		errors = ("Albumname POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = request.POST['albumname']
	try:
		album_object = Album.objects.get(user=user_profile_object, albumname=albumname_in)
	except Album.DoesNotExist:
		errors = ("Albumname does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect('/%s/GalleryPage/' % request.user.username)
		return HttpResponseRedirect('/%s/AdminViewMemberAlbums/%s/' % (
			request.user.username, usernamepost))
	albumname_in = (album_object.albumname)
	if not request.POST['picturename']:
		errors = ("Picture name POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))
	picturename_in = request.POST['picturename']
	try:
		image_object = Images.objects.get(album=album_object, imagename=picturename_in)
	except Images.DoesNotExist:
		errors = ("Picture name does not exist.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))
	picturename_in = (image_object.imagename)
	if not request.POST['status']\
	or not request.POST['margin']\
	or not request.POST['surface']\
	or not request.POST['size']\
	or not request.POST.get('types', False):
		errors = ("Some data POST was empty.")
		messages.error(request, errors)
		if not request.user.is_staff:
			return HttpResponseRedirect(
				'/%s/GalleryPage/AlbumName/%s/' % (request.user.username, albumname_in))
		return HttpResponseRedirect('/%s/AdminViewMemberPicture/%s/%s/' % (
			request.user.username, usernamepost,albumname_in))

	status_in = request.POST['status']
	paper_margin_in = request.POST['margin']
	paper_surface_in = request.POST['surface']
	paper_size_in = request.POST['size']
	paper_type_in = request.POST['types']

	order_object = Log_order.objects.create(
		user=user_profile_object,
		status=status_in,
		paper_margin=paper_margin_in,
		paper_surface=paper_surface_in,
		paper_size=paper_size_in,
		paper_type=paper_type_in,
		payment_is_checked=False,
		payment_is_confirm=False,
		price_perpic=paper_size_in)
	order_object.number_of_photos = 1
	order_object.price_total = int(order_object.get_price_perpic_display())
	order_object.save()
	Images_in_order.objects.create(order=order_object, image=image_object)

	if not request.user.is_staff:
		return HttpResponseRedirect(
			'/%s/OrderPage/' % (request.user.username))
	return HttpResponseRedirect('/%s/AdminViewMemberOrder/%s/' % (
		request.user.username, usernamepost))

##### PAGE LIST ORDER ####
def Page_MemberViewOrderSelf(request, username):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	order_object = Log_order.objects.filter(user=user_profile_object)
	return render_to_response(
		'Order/Member/MemberViewOrderSelf.html',
		{'order': order_object},
		RequestContext(request))

def Page_MemberViewOrderExtend(request, username, date_time):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	datetime_order = request.POST['datetime_order_extend']
	order_object = Log_order.objects.get(user=user_profile_object, date_time=datetime_order)
	orderInOrder_object = Images_in_order.objects.filter(order=order_object)
	return render_to_response(
		'Order/Member/MemberViewExtendOrderSelf.html',
		{'order': orderInOrder_object},
		RequestContext(request))

def Page_MemberViewConfirmOrder(request, username, date_time):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	datetime_order = request.POST['datetime_order_extend']
	order_object = Log_order.objects.get(user=user_profile_object, date_time=datetime_order)
	return render_to_response(
		'Order/Member/MemberViewConfirmOrderDetail.html',
		{'order': order_object},
		RequestContext(request))

def Page_MemberConfirmOrder(request, username, date_time):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	datetime_order = request.POST['datetime_order']
	order_object = Log_order.objects.get(user=user_profile_object, date_time=datetime_order)
	return render_to_response(
		'Order/Member/MemberConfirmOrder.html',
		{'order': order_object,
		"dateTimeForms" : dateTimeForms},
		RequestContext(request))

def Function_MemberConfirmOrder(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')

	user_object = User.objects.get(username=request.user.username)
	user_profile_object = UserProfile.objects.get(user=user_object)
	if not request.POST['order']\
	or not request.POST['date_time']:
		errors = ("Some data POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect(
			'/%s/OrderPage/' % (request.user.username))
	try:
		order_in = request.POST['order']
		dateTimeIn = request.POST['date_time']
		order_object = Log_order.objects.get(user=user_profile_object, date_time=dateTimeIn)
	except Log_order.DoesNotExist:
		errors = ("Datetime dose not exist.")
		messages.error(request, errors)
		return HttpResponseRedirect(
			'/%s/OrderPage/' % (request.user.username))

	if not request.POST['day_paid']\
	or not request.POST['month_paid']\
	or not request.POST['year_paid']\
	or not request.POST['hour_paid']\
	or not request.POST['minute_paid']\
	or not request.POST['cost']:
		errors = ("Some data POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect(
			'/%s/OrderPage/ConfirmOrder/%s/' % (request.user.username, dateTimeIn))

	timeConfirmIn = datetime.datetime.strptime(
		"{}-{}-{} {}:{}".format(
			request.POST['year_paid'],
			request.POST['month_paid'],
			request.POST['day_paid'],
			request.POST['hour_paid'],
			request.POST['minute_paid']
		),"%Y-%m-%d %H:%M")
	costIn = request.POST['cost']

	order_object.payment_is_confirm = True
	order_object.date_time_paid = timeConfirmIn
	order_object.amount_paid = float(costIn)
	order_object.save()
	return HttpResponseRedirect(
	    "/%s/OrderPage/" % (request.user.username))

def Page_AdminViewMemberAlbums(request, username, usernamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	album_object_list = Album.objects.filter(user=user_profile_object)
	return render_to_response('Gallery/Admin/AdminViewMemberAlbum.html',
		{'ALBUMS': album_object_list},
		RequestContext(request))

def Page_AdminViewMemberPicture(request, username, usernamepost, albumnamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	album_object = Album.objects.get(user=user_profile_object, albumname=albumnamepost)
	image_object_list = Images.objects.filter(album=album_object)
	return render_to_response('Gallery/Admin/AdminViewMemberPicture.html',
		{'IMAGES': image_object_list, 'username': usernamepost,'albumname': albumnamepost},
		RequestContext(request))


def Page_AdminViewMemberOrder(request, username, usernamepost):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	order_object_list = Log_order.objects.filter(user=user_profile_object)
	return render_to_response('Order/Admin/AdminViewOrderList.html',
		{'order': order_object_list, 'username': usernamepost},
		RequestContext(request))


def Page_AdminViewOrderListOption(request, username):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	user_object_list = User.objects.all()
	return render_to_response('Order/Admin/AdminViewOrderListOption.html',
		{'paper': PaperChoice, 'listname': user_object_list},
		RequestContext(request))


def Page_AdminViewAllMemberOrder(request, username):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	parameterDict = dict()
	view_option = "<p> Filter by <br>"

	if request.POST.get('select_date'):
		select_date_in = request.POST.get('select_date')
		parameterDict["date_time__gte"] = \
			datetime.datetime.now() - datetime.timedelta(days=int(select_date_in))
		view_option += "- " + select_date_in + " days before <br>"

	elif request.POST.get('select_time'):
		select_time_in = request.POST['select_time']
		parameterDict["date_time__gte"] = \
			datetime.datetime.now() - datetime.timedelta(hours=int(select_time_in))
		view_option += "- " + select_time_in + " hour before <br>"

	if request.POST.get('select_username'):
		select_username_in = request.POST.get('select_username')
		user_object = User.objects.get(username=select_username_in)
		user_profile_object = UserProfile.objects.get(user=user_object)
		parameterDict["user"] = user_profile_object
		view_option += "- username : " + select_username_in + "<br>"

	if request.POST.get('status'):
		select_status_in = request.POST['status']
		parameterDict["status"] = select_status_in
		view_option += "- status : " + RAW_CHOICE_DICT["STATUS"][select_status_in] + "<br>"

	if request.POST.get('price_checked'):
		select_price_checked_in = request.POST['price_checked']
		parameterDict["payment_is_checked"] = True if select_price_checked_in == "Y" else False
		view_option += "- Price checked : " + RAW_CHOICE_DICT["PRICE_CHECKED"][select_price_checked_in] + "<br>"

	if request.POST.get('order_confirmed'):
		select_order_confirmed_in = request.POST['order_confirmed']
		parameterDict["payment_is_confirm"] = True if select_order_confirmed_in == "Y" else False
		view_option += "- Order confirmed : " + RAW_CHOICE_DICT["ORDER_CONFIRMED"][select_order_confirmed_in]

	view_option += "</p>"

	if len(parameterDict) == 0:
		order_object_list = Log_order.objects.all()
		view_option = "All list of order."
	else:
		order_object_list = Log_order.objects.filter(**parameterDict)

	return render_to_response('Order/Admin/AdminViewOrderListAll.html',
		{'order': order_object_list, 'view_option': mark_safe(view_option)},
		RequestContext(request))

def Page_AdminViewOrderListExtend(request, username, usernamepost, date_time):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	order_object_list = Log_order.objects.filter(user=user_profile_object, date_time=date_time)
	image_in_order_object_list = Images_in_order.objects.filter(order=order_object_list)
	return render_to_response('Order/Admin/AdminViewExtendOrder.html',
		{'order': image_in_order_object_list,
		 'username': usernamepost},
		RequestContext(request))

def Page_AdminViewMemberConfirmOrderDetail(request, username, usernamepost, date_time):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	user_object = User.objects.get(username=usernamepost)
	user_profile_object = UserProfile.objects.get(user=user_object)
	order_object = Log_order.objects.get(user=user_profile_object, date_time=date_time)
	return render_to_response('Order/Admin/AdminViewConfirmOrderDetail.html',
		{'order': order_object,
		 'username': usernamepost},
		RequestContext(request))

def Function_AdminCheckPaid(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	if not request.POST['order_username']:
		errors = ("Username order POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AdminViewAllMemberOrder/' % (
			request.user.username))
	order_username_in = request.POST['order_username']
	if not request.POST['order_datetime']:
		errors = ("Datetime order POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AdminViewMemberOrder/%s/' % (
			request.user.username, order_username_in))
	order_datetime_in = request.POST['order_datetime']
	try:
		user_object = User.objects.get(username=order_username_in)
		user_profile_object = UserProfile.objects.get(user=user_object)
	except User.DoesNotExist:
		errors = ("Username order does not exist.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AdminViewAllMemberOrder/' % (
			request.user.username))
	try:
		order_object = Log_order.objects.get(user=user_profile_object, date_time=order_datetime_in)
		order_object.payment_is_checked = True
		order_object.save()
	except Log_order.DoesNotExist:
		errors = ("Datetime order does not exist.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AdminViewMemberOrder/%s/' % (
			request.user.username, order_username_in))

	return HttpResponseRedirect('/%s/AdminViewMemberOrder/%s/' % (
			request.user.username, order_username_in))

def Function_AdminUncheckPaid(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/')
	if not request.user.is_staff:
		return HttpResponseRedirect('/')
	if not request.POST['order_username']:
		errors = ("Username order POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AdminViewAllMemberOrder/' % (
			request.user.username))
	order_username_in = request.POST['order_username']
	if not request.POST['order_datetime']:
		errors = ("Datetime order POST was empty.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AdminViewMemberOrder/%s/' % (
			request.user.username, order_username_in))
	order_datetime_in = request.POST['order_datetime']
	try:
		user_object = User.objects.get(username=order_username_in)
		user_profile_object = UserProfile.objects.get(user=user_object)
	except User.DoesNotExist:
		errors = ("Username order does not exist.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AdminViewAllMemberOrder/' % (
			request.user.username))
	try:
		order_object = Log_order.objects.get(user=user_profile_object, date_time=order_datetime_in)
		order_object.payment_is_checked = False
		order_object.save()
	except Log_order.DoesNotExist:
		errors = ("Datetime order does not exist.")
		messages.error(request, errors)
		return HttpResponseRedirect('/%s/AdminViewMemberOrder/%s/' % (
			request.user.username, order_username_in))

	return HttpResponseRedirect('/%s/AdminViewMemberOrder/%s/' % (
			request.user.username, order_username_in))
