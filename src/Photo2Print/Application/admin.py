# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''

from Application.models import *
from django.contrib import admin

class UserProfileAdmin(admin.ModelAdmin):
    list_display = [ "user", "quota", "permission"]
    list_filter = [ "user", "quota", "permission"]

class ImageAdmin(admin.ModelAdmin):
    list_display = [ "imagename", "imagepath", "width", "height", "dateupload", "filesize",'get_reviews']
    list_filter = ["imagename","width", "height","filesize","album"]
    def get_reviews(self, obj):
	return '%s'%(obj.album.albumname)
    get_reviews.short_description = 'album'

class AlbumAdmin(admin.ModelAdmin):
    list_display = ["albumname","permissionalbum","datecreated","datelastmodify","comment",'get_reviews']
    list_filter = ["albumname","permissionalbum","datecreated","datelastmodify","comment","user"]
    def get_reviews(self, obj):
	return '%s'%(obj.user.user)
    get_reviews.short_description = 'user'

class Log_orderAdmin(admin.ModelAdmin):
    list_display = ["date_time","paper_margin","paper_surface","paper_size",
                    "paper_type","price_perpic","price_total","number_of_photos",
                    "status","date_time_paid","payment_is_checked","payment_is_confirm","amount_paid"]
    list_filter = ["date_time","paper_margin","paper_surface","paper_size",
                   "paper_type","price_perpic","price_total","number_of_photos",
                   "status","date_time_paid","payment_is_checked","payment_is_confirm","amount_paid"]
    def get_reviews(self, obj):
	return '%s'%(obj.user.user)
    get_reviews.short_description = 'user'

admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Images, ImageAdmin)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Log_order, Log_orderAdmin)


