# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_12_2_OrderAlbum(Photo2PrintTestCaseHandler):
    def testcase_12_2_OrderAlbum(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Test login with sample member number 1
		self.loginWithSampleMember1()
		# Go to gallery page
		self.goToGalleryPage()
		# Check not have any albums in gallery
		self.checkNotHaveAnyAlbums()
		# Try add sample album 1
		self.addSampleAlbum1()
		# Upload Sample picture 1
		self.uploadPicture1()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Upload Sample picture 2
		self.uploadPicture2()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Upload Sample picture 3
		self.uploadPicture3()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Upload Sample picture 4
		self.uploadPicture4()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Click back to album
		self.fromUploadPageBackToAlbum1Page()
		# Click to order album
		self.clickOrderAlbum()
		# Select some type
		self.selectOrderPaperTypeKoduk()
		# Click next button on order page
		self.clickNextOrderPage()
		# Select some surface
		self.selectOrderPaperSurfaceGlossy()
		# Click next button on order page
		self.clickNextOrderPage()
		# Select some margin choice
		self.selectOrderPaperMarginNo()
		# Click next button on order page
		self.clickNextOrderPage()
		# Check detail of sample order 2
		self.checkDetailOfSampleOrder2()
        # Click save order
		self.clickSaveOrderPage()
        # Check detail of sample 2 in order page
		self.checkDetailOfSampleOrder2InOderPage()
		# Click Picture in order in order page
		self.clickPictureInOrderInOrderPage()
		# Check detail of sample picture 2 in extend order
		self.checkDetailOfSampleOrder2InOderExtendPage()
		# Logout
		self.allUserLogout()
		# Delete sample member number 1
		self.deleteSampleMember1WithAdmin()
		
if __name__ == "__main__":
    unittest.main()
