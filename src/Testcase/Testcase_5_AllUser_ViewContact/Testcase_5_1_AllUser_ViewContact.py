# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_5_1_ViewContact(Photo2PrintTestCaseHandler):
	def testcase_5_1_viewcontact(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Go to contact page
		self.goToContectPage_CaseNotLogin()
		# Check contact detail
		self.checkContactDetail()
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Test login with sample member number 1
		self.loginWithSampleMember1()
		# Go to contact page
		self.goToContectPage_CaseLoginWithSampleMember1()
		# Check contact detail
		self.checkContactDetail()
		# Logout
		self.allUserLogout()
		# Delete sample member number 1
		self.deleteSampleMember1WithAdmin()
		
if __name__ == "__main__":
	unittest.main()