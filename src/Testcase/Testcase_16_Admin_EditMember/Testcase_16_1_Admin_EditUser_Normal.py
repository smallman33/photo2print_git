# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_16_1_AdminEditUser(Photo2PrintTestCaseHandler):
    def testcase_16_1_adminedituser(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Login with admin
		self.loginWithAdmin()
		# Go to Member list page
		self.goToMemberListPage()
		# Check detail of sample member 1 in member list
		self.checkDetailOfSampleMember1InMemberList()
		# Click to edit smaple member 1
		self.clickToEditSampleMember1InMemberListPage()
		# Edit sample member 1 to sample member 2
		self.adminEditDetailToSampleMember2()
		# Check detail of sample member 2
		self.checkDetailOfSampleMember2InMemberList()
		# Remove sample member 2
		self.clickToDeleteSampleMember2InMemberListPage()

if __name__ == "__main__":
    unittest.main()