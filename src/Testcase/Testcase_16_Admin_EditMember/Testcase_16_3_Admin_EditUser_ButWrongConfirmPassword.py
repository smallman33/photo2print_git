# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_16_3_AdminEditUser(Photo2PrintTestCaseHandler):
    def testcase_16_3_adminedituser(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Login with admin
		self.loginWithAdmin()
		# Go to Member list page
		self.goToMemberListPage()
		# Check detail of sample member 1 in member list
		self.checkDetailOfSampleMember1InMemberList()
		# Click to edit smaple member 1
		self.clickToEditSampleMember1InMemberListPage()
		# Edit detail to sample member 2 with worng confirm password
		self.adminEditDetailToSampleMember2ButWrongConfirmPassword()
		# Check error respond was correct
		self.checkEditDetailRespondWrongConfirmPassword()
		# Go to Member list page
		self.goToMemberListPage()
		# Check detail of sample member 1
		self.checkDetailOfSampleMember1InMemberList()
		# Remove sample member 1
		self.clickToDeleteSampleMember1InMemberListPage()

if __name__ == "__main__":
    unittest.main()