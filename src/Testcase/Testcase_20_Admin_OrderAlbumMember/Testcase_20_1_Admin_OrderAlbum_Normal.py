# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_20_1_AdminOrderAlbum(Photo2PrintTestCaseHandler):
    def testcase_20_1_adminorderalbum(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Test login with sample member number 1
		self.loginWithSampleMember1()
		# Go to gallery page
		self.goToGalleryPage()
		# Check not have any albums in gallery
		self.checkNotHaveAnyAlbums()
		# Try add sample album 1
		self.addSampleAlbum1()
		# Upload Sample picture 1
		self.uploadPicture1()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Upload Sample picture 1
		self.uploadPicture2()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Go to gallery page
		self.goToGalleryPage()
		# Try add sample album 2
		self.addSampleAlbum2()
		# Upload Sample picture 3
		self.uploadPicture3()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Upload Sample picture 4
		self.uploadPicture4()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Logout
		self.allUserLogout()
		# Login with admin
		self.loginWithAdmin()
		# Go to Member list page
		self.goToMemberListPage()
		# Check detail of sample member 1 in member list
		self.checkDetailOfSampleMember1InMemberList()
		# Click to album of sample member 1
		self.clickToAlbumSampleMember1InMemberListPage()
		# Check detail of sample album 1
		self.checkDetailOfSampleAlbum1InMemberAlbumPage()
		# click to order sample album 1
		self.clickToOrderSampleAlbum1InAlbumListPage()
		# Select some type
		self.selectOrderPaperTypeKoduk()
		# Click next button on order page
		self.clickNextOrderPage()
		# Select some surface
		self.selectOrderPaperSurfaceGlossy()
		# Click next button on order page
		self.clickNextOrderPage()
		# Select some margin choice
		self.selectOrderPaperMarginNo()
		# Click next button on order page
		self.clickNextOrderPage()
		# Check detail of sample order 2
		self.checkDetailOfSampleOrder5()
        # Click save order
		self.clickSaveOrderPage()
		# check detail of sample order album 1
		self.checkDetailOrderOfSampleOrder5InMemberOrderPage()
		# Go to Member list page
		self.goToMemberListPage()
		# Check detail of sample member 1
		self.checkDetailOfSampleMember1InMemberList()
		# Remove sample member 1
		self.clickToDeleteSampleMember1InMemberListPage()
		
if __name__ == "__main__":
    unittest.main()
