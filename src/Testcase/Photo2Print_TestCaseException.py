#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''

#######################################################################
# Class Declare
#######################################################################


class  Photo2Print_TestCaseException(Exception):
    ''' Exception of Passbook class
    '''
    def __init__(self, value):
        super(Photo2Print_TestCaseException, self).__init__()
        self.value = value

    def __str__(self):
        return repr(self.value)
