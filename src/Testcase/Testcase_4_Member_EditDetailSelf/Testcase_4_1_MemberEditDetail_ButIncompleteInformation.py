# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_4_1_EditDetail(Photo2PrintTestCaseHandler):
	def testcase_4_1_editdetail(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Test login with sample member number 1
		self.loginWithSampleMember1()
		# Go to detail page
		self.goToSelfDetailPage()
		# Check detail of sample 1 was correct
		self.checkDetailOfSampleMember1()
		# Click Edit detail button
		self.clickEditDetail()
		# Try to edit detail
		self.editDetailToSampleMember2ButEmptyUserName()
		# Check error respond was correct
		self.checkEditDetailRespondEmptyData()
		# Try to edit detail
		self.editDetailToSampleMember2ButEmptyFirstName()
		# Check error respond was correct
		self.checkEditDetailRespondEmptyData()
		# Try to edit detail
		self.editDetailToSampleMember2ButEmptyLastName()
		# Check error respond was correct
		self.checkEditDetailRespondEmptyData()
		# Try to edit detail
		self.editDetailToSampleMember2ButEmptyEmail()
		# Check error respond was correct
		self.checkEditDetailRespondEmptyData()
		# Try to edit detail
		self.editDetailToSampleMember2ButEmptyPassword()
		# Check detail of sample 2 was correct
		self.checkDetailOfSampleMember2()
		# Logout
		self.allUserLogout()
		# Delete sample member number 2
		self.deleteSampleMember2WithAdmin()
		
if __name__ == "__main__":
	unittest.main()