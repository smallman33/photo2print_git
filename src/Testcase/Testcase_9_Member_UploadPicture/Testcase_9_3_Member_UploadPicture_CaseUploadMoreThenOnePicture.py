# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_9_3_UploadPicture(Photo2PrintTestCaseHandler):
    def test_case_9_3_uploadpicture(self):	
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Test login with sample member number 1
		self.loginWithSampleMember1()
		# Go to gallery page
		self.goToGalleryPage()
		# Check not have any albums in gallery
		self.checkNotHaveAnyAlbums()
		# Try add sample album 1
		self.addSampleAlbum1()
		# Upload Sample picture 1
		self.uploadPicture1()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Upload Sample picture 2
		self.uploadPicture2()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Back to Album Page
		self.fromUploadPageBackToAlbum1Page()
		# Click Upload more picture in albumpage
		self.clickUploadMorePictureOnAlbumPage()
		# Upload Sample picture 3
		self.uploadPicture3()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Back to Album Page
		self.fromUploadPageBackToAlbum1Page()
		# Click to sample picture 1 for view full picture
		self.clickPicture3InAlbum1()
		# Check view full picture 1
		self.checkViewFullPicture3()
		# Click Upload more picture in albumpage
		self.clickUploadMorePictureOnFullPicturePage()
		# Upload Sample picture 3
		self.uploadPicture4()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Back to Album Page
		self.fromUploadPageBackToAlbum1Page()
		# Check sample picture 1 was in album page
		self.checkPicture1AlreadyInAlbum1()
		# Check sample picture 2 was in album page
		self.checkPicture2AlreadyInAlbum1()
		# Check sample picture 3 was in album page
		self.checkPicture3AlreadyInAlbum1()
		# Check sample picture 4 was in album page
		self.checkPicture4AlreadyInAlbum1()
		# Logout
		self.allUserLogout()
		# Delete sample member number 1
		self.deleteSampleMember1WithAdmin()
		
if __name__ == "__main__":
	unittest.main()