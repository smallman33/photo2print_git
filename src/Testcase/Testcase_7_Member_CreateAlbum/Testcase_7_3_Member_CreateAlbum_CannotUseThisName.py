# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_7_3_CreateAlbum(Photo2PrintTestCaseHandler):

    def testcase_7_3_createalbum(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Test login with sample member number 1
		self.loginWithSampleMember1()
		# Go to gallery page
		self.goToGalleryPage()
		# Check not have any albums in gallery
		self.checkNotHaveAnyAlbums()
		# Try add sample album 1
		self.addSampleAlbum1()
		# Go to gallery page
		self.goToGalleryPage()
		# Check empty was create
		self.checkHaveEmptyAlbums()
		# Check sample album number 1 was create
		self.checkHaveSampleAlbum1()
		# Click to view sample album1
		self.clickSampleAlbum1()
		# Check detail of sample album number 1 
		self.checkSampleAlbum1Detail()
		# Go to gallery page
		self.goToGalleryPage()
        # Try add sample album 1
		self.addSampleAlbum1Repeatedly()
		# Check error respond was correct
		self.checkAddAlbumRespondAlbumNameExists()
		# Logout
		self.allUserLogout()
		# Delete sample member number 1
		self.deleteSampleMember1WithAdmin()
		
if __name__ == "__main__":
	unittest.main()