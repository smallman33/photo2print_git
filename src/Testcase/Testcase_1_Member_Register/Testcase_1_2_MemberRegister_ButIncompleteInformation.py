# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_1_2_Register(Photo2PrintTestCaseHandler):

	def testcase_1_2_register(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Try to register with sample member number 1
		self.registerWithSampleMember1ButWithoutFirstName()
		# Check error respond was correct
		self.checkRegisterRespondEmptyData()
		# Try to register with sample member number 1
		self.registerWithSampleMember1ButWithoutLastName()
		# Check error respond was correct
		self.checkRegisterRespondEmptyData()
		# Try to register with sample member number 1
		self.registerWithSampleMember1ButWithoutUserName()
		# Check error respond was correct
		self.checkRegisterRespondEmptyData()
		# Try to register with sample member number 1
		self.registerWithSampleMember1ButWithoutEmail()
		# Check error respond was correct
		self.checkRegisterRespondEmptyData()
		# Try to register with sample member number 1
		self.registerWithSampleMember1ButWithoutPassword()
		# Check error respond was correct
		self.checkRegisterRespondEmptyData()

if __name__ == "__main__":
	unittest.main()