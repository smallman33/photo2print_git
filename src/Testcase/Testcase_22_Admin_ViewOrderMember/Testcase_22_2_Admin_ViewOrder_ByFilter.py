# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_22_2_ViewOrder(Photo2PrintTestCaseHandler):
    def testcase_22_2_vieworder(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Test login with sample member number 1
		self.loginWithSampleMember1()
		# Go to gallery page
		self.goToGalleryPage()
		# Check not have any albums in gallery
		self.checkNotHaveAnyAlbums()
		# Try add sample album 1
		self.addSampleAlbum1()
		# Upload Sample picture 1
		self.uploadPicture1()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Back to album page
		self.fromUploadPageBackToAlbumPage()
		# Click to sample picture 1 for view full picture
		self.clickPicture1InAlbum1()
		# Check view full picture 1
		self.checkViewFullPicture1()
		# Click to order album
		self.clickOrderPicture()
		# Select some type
		self.selectOrderPaperTypeFuji()
		# Click next button on order page
		self.clickNextOrderPage()
		# Select some surface
		self.selectOrderPaperSurfaceGlossy()
		# Click next button on order page
		self.clickNextOrderPage()
		# Select some margin choice
		self.selectOrderPaperMarginYes()
		# Click next button on order page
		self.clickNextOrderPage()
		# Check detail of sample order 1
		self.checkDetailOfSampleOrder4()
        # Click save order
		self.clickSaveOrderPage()
        # Check detail of sample 2 in order page
		self.checkDetailOfSampleOrder4InOderPage()
		# Click Picture in order in order page
		self.clickPictureInOrderInOrderPage()
		# Check detail of sample picture 2 in extend order
		self.checkDetailOfSampleOrder4InOderExtendPage()
		# Logout
		self.allUserLogout()
		# Login with admin
		self.loginWithAdmin()
        # Go to Order list page
		self.goToOrderListPage()
        # Check detail of sample order 4
		self.checkDetailOfSampleOrder4InOderListPage()
		# Go to Member list page
		self.goToMemberListPage()
		# Check detail of sample member 1
		self.checkDetailOfSampleMember1InMemberList()
		# Remove sample member 1
		self.clickToDeleteSampleMember1InMemberListPage()

if __name__ == "__main__":
    unittest.main()