# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_10_2_DeleteAlbum(Photo2PrintTestCaseHandler):
    def testcase_10_2_deletealbum(self):	
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Test login with sample member number 1
		self.loginWithSampleMember1()
		# Go to gallery page
		self.goToGalleryPage()
		# Check not have any albums in gallery
		self.checkNotHaveAnyAlbums()
		# Try add sample album 1
		self.addSampleAlbum1()
		# Upload Sample picture 1
		self.uploadPicture1()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Upload Sample picture 2
		self.uploadPicture2()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Go to gallery page
		self.goToGalleryPage()
		# Try add sample album 1
		self.addSampleAlbum2()
		# Upload Sample picture 1
		self.uploadPicture3()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Upload Sample picture 2
		self.uploadPicture4()
		# Check upload picture success
		self.checkUploadPictureSuccess()
		# Go to gallery page
		self.goToGalleryPage()
		# Go to sample album 1
		self.clickSampleAlbum1()
		# Click Delete this album
		self.clickDeleteThisAlbum()
        # Go to sample album 1
		self.clickSampleAlbum2()
		# Click Delete this album
		self.clickDeleteThisAlbum()
        # Check not have any albums in gallery
		self.checkNotHaveAnyAlbums()
		# Logout
		self.allUserLogout()
		# Delete sample member number 1
		self.deleteSampleMember1WithAdmin()
		
if __name__ == "__main__":
	unittest.main()