# !/usr/bin/python
# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import unittest
import urllib
import time
import datetime
import dateutil.parser
import os
import zipfile
from Photo2Print_TestCaseException import Photo2Print_TestCaseException

class Photo2PrintTestCaseHandler(unittest.TestCase):
	'''
	'''
# Initial
	def setUp(self):
		self.base_url = r"http://smallman33.pythonanywhere.com"
		self.verificationErrors = []
		self.accept_next_alert = True

		self.memberUsername = u"Nanio"
		self.memberPassword = "230633"
		self.memberFirstname = "Anan"
		self.memberLastname = "Kamkemkeaw"
		self.memberEmail = "nannoy_hup@hotmail.com"
		self.memberPermission = "Member"

		self.memberNewUsername = "Kwangnoi"
		self.memberNewPassword = "191233"
		self.memberNewFirstname = "Satita"
		self.memberNewLastname = "Phuriyakorn"
		self.memberNewEmail = "smallman.developer@gmail.com"
		self.memberNewPermissionChoice = "A"
		self.memberNewPermission = "Administrator"

		self.adminUsername = "adminPhoto"
		self.adminPassword = "230633"

		self.contactPhoneNumber = "Tel. 082-780-4115"
		self.contactEmail = "E-mail : nannoy_hup@hotmail.com"

		self.album1Name = u"ไปหัวหิน"
		self.album1Comment = u"ไปเจอหอย"
		self.album1Permission = 'PR'
		self.album2Name = u"ไปดอยอินทนนท์"
		self.album2Comment = u"ไม่มีหอย"
		self.album2Permission = 'PU'

		self.samplePicture1Name = "pearls.jpg"
		self.samplePicture2Name = "shells.jpg"
		self.samplePicture3Name = "DoyIntanon_Jade.jpg"
		self.samplePicture4Name = "DoyIntanon_Sign.jpg"
		self.downloadFileName = None
		self.waittingTime = 3
		self.driverWaittingTime = 60
		self.reloadWaittingTime = 2
		self.wait = None
		
	def setWebDrive(self, webDrive):
		self.driver = webDrive
		# Waitting for load webdrive
		time.sleep(4)
		self.driver.set_page_load_timeout(self.driverWaittingTime)
		self.wait = WebDriverWait(self.driver, self.driverWaittingTime)
		self.driver.get(u"{}/".format(self.base_url))
		self.driver.find_element_by_css_selector("section.jFlowNext").click()

# Test method
	def isAlertPresent(self):
		try:
			self.driver.switch_to_alert()
		except NoAlertPresentException:
			return False
		return True

	def closeAlertAndGetItsText(self):
		try:
			alert = self.driver.switch_to_alert()
			alert_text = alert.text
			if self.accept_next_alert:
				alert.accept()
			else:
				alert.dismiss()
			return alert_text
		finally:
			self.accept_next_alert = True

	def tearDown(self):
		self.driver.quit()
		self.assertEqual([], self.verificationErrors)
	def busyLoopWaitUrl(self, urlCandidate):
		current_time = time.time()
		urlCurrent = None
		while (time.time() < (current_time + self.driverWaittingTime)):
			urlCurrent = Photo2PrintTestCaseHandler.convert_utf8_url_to_unicode_url(
				self.driver.current_url)
			if (urlCurrent == urlCandidate):
				return True
		raise Photo2Print_TestCaseException( 
			u"Timeout watting for urls change. URL : {} != {}".format(
				urlCurrent,
				urlCandidate
			))

	def busyLoopWaitAlert(self):
		current_time = time.time()
		while (time.time() < (current_time + self.driverWaittingTime)):
			if (self.isAlertPresent()):
				return True
		raise Photo2Print_TestCaseException( 
			u"Timeout watting for alert")

	def busyLoopWaitDownloadOrder(self):
		current_time = time.time()
		downloadPath = os.path.abspath(
			os.path.join(os.path.dirname( __file__ ), 'download_dir',self.downloadFileName))
		
		while (time.time() < (current_time + self.driverWaittingTime)):
			if (os.path.exists(downloadPath)):
				if not (os.path.exists(downloadPath + ".part")):
					return True
		raise Photo2Print_TestCaseException( 
			u"Timeout watting for Download")

	@staticmethod
	def convert_utf8_url_to_unicode_url(utf8_url):
		return urllib.unquote(str(utf8_url)).decode("utf-8")


# Register
	def registerWithSampleMember1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Click Register submit
		registerInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Register']")))
		registerInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl("{}/{}/".format(
			self.base_url,
			r"RegisterPage"))
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element located
		firstnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, '//input[@id="register" and @name="firstname"]')))
		# Input sample value
		self.driver.find_element_by_xpath('//input[@id="register" and @name="firstname"]').clear()
		self.driver.find_element_by_xpath('//input[@id="register" and @name="firstname"]').send_keys(self.memberFirstname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").send_keys(self.memberLastname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").send_keys(self.memberUsername)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").send_keys(self.memberEmail)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @value='Register']").click()

	def registerWithSampleMember1ButWithoutFirstName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Click Register submit
		registerInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Register']")))
		registerInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"RegisterPage"))
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element located
		firstnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@id='register' and @name='firstname']")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").send_keys("")
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").send_keys(self.memberLastname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").send_keys(self.memberUsername)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").send_keys(self.memberEmail)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @value='Register']").click()
		
	def registerWithSampleMember1ButWithoutLastName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Click Register submit
		registerInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Register']")))
		registerInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"RegisterPage"))
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element located
		firstnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@id='register' and @name='firstname']")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").send_keys(self.memberFirstname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").send_keys("")
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").send_keys(self.memberUsername)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").send_keys(self.memberEmail)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @value='Register']").click()

	def registerWithSampleMember1ButWithoutUserName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Click Register submit
		registerInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Register']")))
		registerInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"RegisterPage"))
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element located
		firstnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@id='register' and @name='firstname']")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").send_keys(self.memberFirstname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").send_keys(self.memberLastname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").send_keys("")
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").send_keys(self.memberEmail)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @value='Register']").click()

	def registerWithSampleMember1ButWithoutEmail(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Click Register submit
		registerInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Register']")))
		registerInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"RegisterPage"))
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element located
		firstnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@id='register' and @name='firstname']")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").send_keys(self.memberFirstname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").send_keys(self.memberLastname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").send_keys(self.memberUsername)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").send_keys("")
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @value='Register']").click()

	def registerWithSampleMember1ButWithoutPassword(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Click Register submit
		registerInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Register']")))
		registerInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"RegisterPage"))
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element located
		firstnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@id='register' and @name='firstname']")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").send_keys(self.memberFirstname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").send_keys(self.memberLastname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").send_keys(self.memberUsername)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").send_keys(self.memberEmail)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").send_keys("")
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").send_keys("")
		self.driver.find_element_by_xpath("//input[@id='register' and @value='Register']").click()

	def registerWithSampleMember1ButWrongConfirmPassword(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Click Register submit
		registerInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Register']")))
		registerInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"RegisterPage"))
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element located
		firstnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@id='register' and @name='firstname']")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='firstname']").send_keys(self.memberFirstname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='lastname']").send_keys(self.memberLastname)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='username']").send_keys(self.memberUsername)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='e_mail']").send_keys(self.memberEmail)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password']").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").clear()
		self.driver.find_element_by_xpath("//input[@id='register' and @name='password_c']").send_keys(self.memberPassword + 'r')
		self.driver.find_element_by_xpath("//input[@id='register' and @value='Register']").click()

	def checkRegisterRespondSuccess(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(str(respondElement.text), "Register is success.")

	def checkRegisterRespondCannotUsedThisName(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(str(respondElement.text), "This username is can't be use")

	def checkRegisterRespondEmptyData(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(str(respondElement.text), "Empty data or some data is NULL.")

	def checkRegisterRespondWrongConfirmPassword(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(str(respondElement.text), "Your password and comfirm password didn't match. Please try again.")

# Login
	def checkUrlIsLoginPage(self):
		# Waitting url for change
		self.busyLoopWaitUrl("{}/{}/".format(
			self.base_url,
			r"LoginPage"))

	def loginWithSampleMember1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@name=\"username\"]")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys(self.memberUsername)
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@value='Login']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r"HomePage"))

	def loginWithSampleMember1ButNotInputUserName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@name=\"username\"]")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys("")
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@value='Login']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"LoginPage"))

	def loginWithSampleMember1ButNotInputPassword(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@name=\"username\"]")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys(self.memberUsername)
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys("")
		self.driver.find_element_by_xpath("//input[@value='Login']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			"LoginPage"))

	def loginWithSampleMember1ButNotWrongUserName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@name=\"username\"]")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys(self.memberUsername + "r")
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys(self.memberPassword)
		self.driver.find_element_by_xpath("//input[@value='Login']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"LoginPage"))

	def loginWithSampleMember1ButNotWrongPassword(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@name=\"username\"]")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys(self.memberUsername)
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys(self.memberPassword + "r")
		self.driver.find_element_by_xpath("//input[@value='Login']").click()
		time.sleep(self.waittingTime)
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			r"LoginPage"))

	def checkLoginRespondEmptyData(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(str(respondElement.text), "Empty data or some data is NULL.")

	def checkLoginRespondWrongUserNameOrPassword(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(str(respondElement.text), "Your username and password didn't match. Please try again.")

	def allUserLogout(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for logout input element is located
		logoutInputelement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//input[@value="Logout"]')))
		logoutInputelement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"HomePage"))

# Detail
	def goToSelfDetailPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		memberListLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='" + self.memberUsername + "']")))
		memberListLinkElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r"SelfDetailPage"))

	def checkDetailOfSampleMember1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		username = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[1]").text
		self.assertEqual(username, "Username : " + self.memberUsername)
		firstname = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[2]").text
		self.assertEqual(firstname, "Firstname : " + self.memberFirstname)
		lastname = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[3]").text
		self.assertEqual(lastname, "Lastname  : " + self.memberLastname)
		permission = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[4]").text
		self.assertEqual(permission, "Staff           : False")
		email = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[5]").text
		self.assertEqual(email, "E_mail        : " + self.memberEmail)

	def checkDetailOfSampleMember2(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		username = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[1]").text
		self.assertEqual(username, "Username : " + self.memberNewUsername)
		firstname = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[2]").text
		self.assertEqual(firstname, "Firstname : " + self.memberNewFirstname)
		lastname = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[3]").text
		self.assertEqual(lastname, "Lastname  : " + self.memberNewLastname)
		permission = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[4]").text
		self.assertEqual(permission, "Staff           : False")
		email = self.driver.find_element_by_xpath("//div[@id='detail-user']/h3[5]").text
		self.assertEqual(email, "E_mail        : " + self.memberNewEmail)

	def clickEditDetail(self):
		# Waitting for element was located
		editDetailInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Edit detail']")))
		editDetailInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r'EditUserDetail',
			self.memberUsername))

	def editDetailToSampleMember2(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input sample value
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberNewUsername,
			r"SelfDetailPage"))

	def editDetailToSampleMember2ButEmptyUserName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input sample value
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys("")
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

	def editDetailToSampleMember2ButEmptyFirstName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input sample value
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys("")
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

	def editDetailToSampleMember2ButEmptyLastName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input sample value
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys("")
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

	def editDetailToSampleMember2ButEmptyEmail(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input sample value
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys("")
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

	def editDetailToSampleMember2ButEmptyPassword(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input sample value
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys("")
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys("")
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberNewUsername,
			r"SelfDetailPage"))

	def editDetailToSampleMember2ButWrongConfirmPassword(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input sample value
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword + "r")
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(
			self.base_url +\
			"EditUserDetailFunction/")

	def checkEditDetailRespondEmptyData(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(
			str(respondElement.text), 
			"Empty data or some data is NULL.")

	def checkEditDetailRespondWrongConfirmPassword(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(
			str(respondElement.text), 
			"Your password and comfirm password didn't match. Please try again.")

# View contact
	def goToContectPage_CaseNotLogin(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		memberListLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//img[@src="/media/images/contact.png"]')))
		memberListLinkElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"ContactPage"))

	def goToContectPage_CaseLoginWithSampleMember1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		memberListLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//img[@src="/media/images/contact.png"]')))
		memberListLinkElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r"ContactPage"))

	def checkContactDetail(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE
		phoneNumber = self.driver.find_element_by_xpath("//div[@id='contact']/h3[1]").text
		self.assertEqual(phoneNumber, self.contactPhoneNumber)
		email = self.driver.find_element_by_xpath("//div[@id='contact']/h3[2]").text
		self.assertEqual(email, self.contactEmail)

# Manage Album
	def goToGalleryPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		galleryPageLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//img[@src="/media/images/gallary.png"]')))
		galleryPageLinkElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r'GalleryPage'))

	def checkNotHaveAnyAlbums(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		notHaveAlbumLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//img[@src="/media/images/nothave.jpg"]')))

	def checkHaveEmptyAlbums(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		notHaveAlbumLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//img[@src="/media/images/picnull.jpeg"]')))

	def addSampleAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		createAlbumInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Create Albums']")))
		createAlbumInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'AddAlbumPage'))

		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		albumnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "album")))
		# Input sample value
		self.driver.find_element_by_name("album").clear()
		self.driver.find_element_by_name("album").send_keys(self.album1Name)
		self.driver.find_elements_by_xpath(
			".//input[@type='radio' and @value='"+self.album1Permission+"']")[0].click()
		self.driver.find_element_by_name("comment").clear()
		self.driver.find_element_by_name("comment").send_keys(self.album1Comment)
		self.driver.find_element_by_xpath("//input[@value='Add Albums']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name,
			u"UploadPicturePage"))

	def addSampleAlbum1WithEmptyAlbumName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		createAlbumInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Create Albums']")))
		createAlbumInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'AddAlbumPage'))

		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		albumnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "album")))
		# Input sample value
		self.driver.find_element_by_name("album").clear()
		self.driver.find_element_by_name("album").send_keys("")
		self.driver.find_elements_by_xpath(
			".//input[@type='radio' and @value='"+self.album1Permission+"']")[0].click()
		self.driver.find_element_by_name("comment").clear()
		self.driver.find_element_by_name("comment").send_keys(self.album1Comment)
		self.driver.find_element_by_xpath("//input[@value='Add Albums']").click()
		# Check url not change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'AddAlbumPage'))

	def addSampleAlbum1WithEmptyComment(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		createAlbumInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Create Albums']")))
		createAlbumInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'AddAlbumPage'))

		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		albumnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "album")))
		# Input sample value
		self.driver.find_element_by_name("album").clear()
		self.driver.find_element_by_name("album").send_keys(self.album1Name)
		self.driver.find_elements_by_xpath(".//input[@type='radio' and @value='"+self.album1Permission+"']")[0].click()
		time.sleep(self.waittingTime)
		self.driver.find_element_by_name("comment").clear()
		self.driver.find_element_by_name("comment").send_keys("")
		self.driver.find_element_by_xpath("//input[@value='Add Albums']").click()
		# Check url not change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'AddAlbumPage'))

	def addSampleAlbum1WithoutSelectPermission(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		createAlbumInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Create Albums']")))
		createAlbumInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'AddAlbumPage'))

		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		albumnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "album")))
		# Input sample value
		self.driver.find_element_by_name("album").clear()
		self.driver.find_element_by_name("album").send_keys(self.album1Name)
		self.driver.find_element_by_name("comment").clear()
		self.driver.find_element_by_name("comment").send_keys(self.album1Comment)
		self.driver.find_element_by_xpath("//input[@value='Add Albums']").click()
		# Check url not change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'AddAlbumPage'))

	def addSampleAlbum1Repeatedly(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		createAlbumInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Create Albums']")))
		createAlbumInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'AddAlbumPage'))

		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		albumnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "album")))
		# Input sample value
		self.driver.find_element_by_name("album").clear()
		self.driver.find_element_by_name("album").send_keys(self.album1Name)
		self.driver.find_elements_by_xpath(
			".//input[@type='radio' and @value='"+self.album1Permission+"']")[0].click()
		self.driver.find_element_by_name("comment").clear()
		self.driver.find_element_by_name("comment").send_keys(self.album1Comment)
		self.driver.find_element_by_xpath("//input[@value='Add Albums']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'AddAlbumPage'))

	def addSampleAlbum2(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		createAlbumInputElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Create Albums']")))
		createAlbumInputElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r'AddAlbumPage'))

		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		albumnameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "album")))
		# Input sample value
		self.driver.find_element_by_name("album").clear()
		self.driver.find_element_by_name("album").send_keys(self.album2Name)
		self.driver.find_elements_by_xpath(
			".//input[@type='radio' and @value='"+self.album2Permission+"']")[0].click()
		self.driver.find_element_by_name("comment").clear()
		self.driver.find_element_by_name("comment").send_keys(self.album2Comment)
		self.driver.find_element_by_xpath("//input[@value='Add Albums']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r'GalleryPage',
			r'AlbumName',
			self.album2Name,
			r"UploadPicturePage"))

	def checkAddAlbumRespondEmptyData(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']")))
		self.assertEqual(str(respondElement.text), "Empty Data.")

	def checkAddAlbumRespondAlbumNameExists(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']")))
		self.assertEqual(str(respondElement.text), "Albumname exists.")

	def checkHaveSampleAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		sampleAlbum1LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, u'//a[@href="/Nanio/GalleryPage/AlbumName/'+ self.album1Name+'/"]')))

	def checkHaveSampleAlbum2(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		sampleAlbum2LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, u'//a[@href="/Nanio/GalleryPage/AlbumName/'+ self.album2Name+'/"]')))
	
	def clickSampleAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		sampleAlbum1LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, u'//a[@href="/Nanio/GalleryPage/AlbumName/'+ self.album1Name+'/"]')))
		sampleAlbum1LinkElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r'GalleryPage',
			r'AlbumName',
			self.album1Name))

	def clickSampleAlbum2(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		sampleAlbum2LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, u'//a[@href="/Nanio/GalleryPage/AlbumName/'+ self.album2Name+'/"]')))
		sampleAlbum2LinkElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r'GalleryPage',
			r'AlbumName',
			self.album2Name))

	def checkSampleAlbum1Detail(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		albumnameTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='album-name']/h5")))
		albumname = self.driver.find_element_by_xpath("//div[@id='album-name']/h5").text
		self.assertEqual(albumname, self.album1Name)
		comment = self.driver.find_element_by_xpath("//textarea[@name='comment']").text
		self.assertEqual(comment, self.album1Comment)

	def checkSampleAlbum2Detail(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		albumnameTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='album-name']/h5")))
		albumname = self.driver.find_element_by_xpath("//div[@id='album-name']/h5").text
		self.assertEqual(albumname, self.album2Name)
		comment = self.driver.find_element_by_xpath("//textarea[@name='comment']").text
		self.assertEqual(comment, self.album2Comment)

	def clickDeleteThisAlbum(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		sampleAlbum1LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@value='Delete this album']")))
		sampleAlbum1LinkElement.click()
		# Waitting for alert
		self.busyLoopWaitAlert()
		self.assertRegexpMatches(
			self.closeAlertAndGetItsText(), 
			"^Are you sure to delete this album[\\s\\S]\nAll pictures in this album will be remove too\\.$")
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r'GalleryPage'))

# Manage Picture
	def uploadPicture1(self):
		# self.driver.refresh()
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		fileInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@type='file']")))
		fileInputElement.send_keys(
			os.path.abspath(
				os.path.join(
					"SamplePictures",
					self.samplePicture1Name)))
		self.driver.find_element_by_xpath(
			"//input[@value='Upload']").click()	

	def uploadPicture2(self):
		# self.driver.refresh()
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		fileInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@type='file']")))
		fileInputElement.send_keys(
			os.path.abspath(
				os.path.join(
					"SamplePictures",
					self.samplePicture2Name)))
		self.driver.find_element_by_xpath(
			"//input[@value='Upload']").click()	

	def uploadPicture3(self):
		# self.driver.refresh()
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		fileInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@type='file']")))
		fileInputElement.send_keys(
			os.path.abspath(
				os.path.join(
					"SamplePictures",
					self.samplePicture3Name)))
		self.driver.find_element_by_xpath(
			"//input[@value='Upload']").click()	

	def uploadPicture4(self):
		# self.driver.refresh()
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		fileInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@type='file']")))
		fileInputElement.send_keys(
			os.path.abspath(
				os.path.join(
					"SamplePictures",
					self.samplePicture4Name)))
		self.driver.find_element_by_xpath(
			"//input[@value='Upload']").click()

	def uploadPictureWithoutSelectPicture(self):
		#####Upload Picture 1
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		self.driver.find_element_by_xpath(
			"//input[@value='Upload']").click()	

	def clickUploadMorePictureOnAlbumPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		uploadMorePictureElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='+ Upload more picture']")))
		uploadMorePictureElement.click()

	def clickUploadMorePictureOnFullPicturePage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		uploadMorePictureElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='+ Upload more picture']")))
		uploadMorePictureElement.click()
		
	def checkUploadPictureSuccess(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(
			str(respondElement.text),
			"Complete.")

	def checkUploadPictureRespondEmptyData(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(
			str(respondElement.text),
			"Empty data or some data is NULL.")

	def checkUploadPictureRespondRepetitivePictureName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(
			str(respondElement.text),
			"Image name this Repeatedly.")

	def fromUploadPageBackToAlbum1Page(self):
		# Checking current url is correct
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name,
			u"UploadPicturePage"))
		# Waitting for element was located
		backToAlbumButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Back to Album']")))
		backToAlbumButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name))

	def checkNotHaveAnyPictureInAlbum(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		self.driver.find_element_by_xpath('//img[@src="/media/images/nothavepic.jpg"]')

	def checkPicture1AlreadyInAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		self.driver.find_element_by_xpath(
			u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
				self.memberUsername,
				self.album1Name,
				self.samplePicture1Name))

	def checkPicture2AlreadyInAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		self.driver.find_element_by_xpath(
			u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
				self.memberUsername,
				self.album1Name,
				self.samplePicture2Name))

	def checkPicture3AlreadyInAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		self.driver.find_element_by_xpath(
			u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
				self.memberUsername,
				self.album1Name,
				self.samplePicture3Name))

	def checkPicture4AlreadyInAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		self.driver.find_element_by_xpath(
			u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
				self.memberUsername,
				self.album1Name,
				self.samplePicture4Name))

	def clickPicture1InAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		samplePicture1LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
					self.memberUsername,
					self.album1Name,
					self.samplePicture1Name))))
		samplePicture1LinkElement.click()

	def clickPicture2InAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		samplePicture2LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
					self.memberUsername,
					self.album1Name,
					self.samplePicture2Name))))
		samplePicture2LinkElement.click()

	def clickPicture3InAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		samplePicture3LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
					self.memberUsername,
					self.album1Name,
					self.samplePicture3Name))))
		samplePicture3LinkElement.click()

	def clickPicture4InAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		samplePicture4LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
					self.memberUsername,
					self.album1Name,
					self.samplePicture4Name))))
		samplePicture4LinkElement.click()

	def checkViewFullPicture1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for respond element located
		pictureNameElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='picture-name']/h4")))
		self.assertEqual(
			pictureNameElement.text,
			self.samplePicture1Name)

	def checkViewFullPicture2(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for respond element located
		pictureNameElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='picture-name']/h4")))
		self.assertEqual(
			pictureNameElement.text,
			self.samplePicture2Name)

	def checkViewFullPicture3(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for respond element located
		pictureNameElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='picture-name']/h4")))
		self.assertEqual(
			pictureNameElement.text,
			self.samplePicture3Name)

	def checkViewFullPicture4(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for respond element located
		pictureNameElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='picture-name']/h4")))
		self.assertEqual(
			pictureNameElement.text,
			self.samplePicture4Name)

	def fromFullPicture1PageBackToAlbum1Page(self):
		# Checking current url is correct
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name,
			u'PictureName',
			self.samplePicture1Name))
		# Waitting for element was located
		backToAlbumButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Back to Album']")))
		backToAlbumButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name))

	def fromFullPicture2PageBackToAlbum1Page(self):
		# Checking current url is correct
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name,
			u'PictureName',
			self.samplePicture2Name))
		# Waitting for element was located
		backToAlbumButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Back to Album']")))
		backToAlbumButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name))

	def fromFullPicture3PageBackToAlbum1Page(self):
		# Checking current url is correct
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name,
			u'PictureName',
			self.samplePicture3Name))
		# Waitting for element was located
		backToAlbumButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Back to Album']")))
		backToAlbumButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name))

	def fromFullPicture4PageBackToAlbum1Page(self):
		# Checking current url is correct
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name,
			u'PictureName',
			self.samplePicture4Name))
		# Waitting for element was located
		backToAlbumButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Back to Album']")))
		backToAlbumButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			u'GalleryPage',
			u'AlbumName',
			self.album1Name))

	def clickDeleteThisPicture(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		sampleAlbum1LinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@value='Delete this image']")))
		sampleAlbum1LinkElement.click()
		# Waitting for alert
		self.busyLoopWaitAlert()
		self.assertRegexpMatches(
			self.closeAlertAndGetItsText(), 
			r"^Are you sure to delete this Picture [\s\S]$")
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.memberUsername,
			r'GalleryPage',
			r"AlbumName",
			self.album1Name))

	def checkHavePicture1AsCoverAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for respond element located
		pictureNameElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, 
				u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
					self.memberUsername,
					self.album1Name,
					self.samplePicture1Name))))
	
	def checkHavePicture2AsCoverAlbum1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for respond element located
		pictureNameElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, 
				u'//img[@src="/media/Account/{}/{}/Thumpnail/{}"]'.format(
					self.memberUsername,
					self.album1Name,
					self.samplePicture2Name))))
	
	def checkHaveNonePicAsCoverAlbum(self):
		pictureNameElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, 
				u'//img[@src="/media/images/picnull.jpeg"]')))

# Order
	def clickOrderAlbum(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click Add album
		orderAlbumButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Add this album to print']")))
		orderAlbumButtonElement.click()

	def clickOrderPicture(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click Add album
		orderAlbumButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Add this image to print']")))
		orderAlbumButtonElement.click()

	def clickNextOrderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click Add album
		nextButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Next']")))
		nextButtonElement.click()

	def clickSaveOrderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click Add album
		saveButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Save']")))
		saveButtonElement.click()

	def checkOrderRespondMustSelectChoice(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(str(respondElement.text), "Must select some choice.")

	def selectOrderPaperTypeSuperDigital(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click type choice
		paperTypeButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='S']")))
		paperTypeButtonElement.click()
		
	def selectOrderPaperTypeKoduk(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click type choice
		paperTypeButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='K']")))
		paperTypeButtonElement.click()
		
	def selectOrderPaperTypeFuji(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click type choice
		paperTypeButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='F']")))
		paperTypeButtonElement.click()
		
	def selectOrderPaperSurfaceMatt(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click surface choice
		paperSurfaceButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='M']")))
		paperSurfaceButtonElement.click()
		
	def selectOrderPaperSurfaceGlossy(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click surface choice
		paperSurfaceButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='G']")))
		paperSurfaceButtonElement.click()

	def selectOrderPaperMarginYes(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click margin choice
		paperSurfaceButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='Y']")))
		paperSurfaceButtonElement.click()
		
	def selectOrderPaperMarginNo(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click margin choice
		paperSurfaceButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='N']")))
		paperSurfaceButtonElement.click()

	def checkDetailOfSampleOrder1(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		marginTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH,
				"//div[@id='order_paper_margin']/h6")))
		paper_margin = self.driver.find_element_by_xpath("//div[@id='order_paper_margin']/h6").text
		self.assertEqual(str(paper_margin), "YES")
		paper_type = self.driver.find_element_by_xpath("//div[@id='order_paper_type']/h6").text
		self.assertEqual(str(paper_type), "Super Digital Professional")
		paper_surface = self.driver.find_element_by_xpath("//div[@id='order_paper_surface']/h6").text
		self.assertEqual(str(paper_surface), "Matt")
		paper_size = self.driver.find_element_by_xpath("//div[@id='order_paper_size']/h6").text
		self.assertEqual(str(paper_size), "3.5x5")
		paper_amount = self.driver.find_element_by_xpath("//div[@id='order_amount_picture']/h6").text
		self.assertEqual(str(paper_amount), "4 Picture")
		paper_price = self.driver.find_element_by_xpath("//div[@id='order_price_per_picture']/h6").text
		self.assertEqual(str(paper_price), "3 Baht")
		paper_price_total = self.driver.find_element_by_xpath("//div[@id='order_price_total']/h6").text
		self.assertEqual(str(paper_price_total), "12 Baht")

	def checkDetailOfSampleOrder2(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		marginTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH,
				"//div[@id='order_paper_margin']/h6")))
		paper_margin = self.driver.find_element_by_xpath("//div[@id='order_paper_margin']/h6").text
		self.assertEqual(str(paper_margin), "NO")
		paper_type = self.driver.find_element_by_xpath("//div[@id='order_paper_type']/h6").text
		self.assertEqual(str(paper_type), "Koduk Royal")
		paper_surface = self.driver.find_element_by_xpath("//div[@id='order_paper_surface']/h6").text
		self.assertEqual(str(paper_surface), "glossy")
		paper_size = self.driver.find_element_by_xpath("//div[@id='order_paper_size']/h6").text
		self.assertEqual(str(paper_size), "2P")
		paper_amount = self.driver.find_element_by_xpath("//div[@id='order_amount_picture']/h6").text
		self.assertEqual(str(paper_amount), "4 Picture")
		paper_price = self.driver.find_element_by_xpath("//div[@id='order_price_per_picture']/h6").text
		self.assertEqual(str(paper_price), "8 Baht")
		paper_price_total = self.driver.find_element_by_xpath("//div[@id='order_price_total']/h6").text
		self.assertEqual(str(paper_price_total), "32 Baht")

	def checkDetailOfSampleOrder3(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		marginTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH,
				"//div[@id='order_paper_margin']/h6")))
		paper_margin = self.driver.find_element_by_xpath("//div[@id='order_paper_margin']/h6").text
		self.assertEqual(str(paper_margin), "YES")
		paper_type = self.driver.find_element_by_xpath("//div[@id='order_paper_type']/h6").text
		self.assertEqual(str(paper_type), "Fuji")
		paper_surface = self.driver.find_element_by_xpath("//div[@id='order_paper_surface']/h6").text
		self.assertEqual(str(paper_surface), "Matt")
		paper_size = self.driver.find_element_by_xpath("//div[@id='order_paper_size']/h6").text
		self.assertEqual(str(paper_size), "4x6")
		paper_amount = self.driver.find_element_by_xpath("//div[@id='order_amount_picture']/h6").text
		self.assertEqual(str(paper_amount), "1 Picture")
		paper_price = self.driver.find_element_by_xpath("//div[@id='order_price_per_picture']/h6").text
		self.assertEqual(str(paper_price), "3 Baht")
		paper_price_total = self.driver.find_element_by_xpath("//div[@id='order_price_total']/h6").text
		self.assertEqual(str(paper_price_total), "3 Baht")

	def checkDetailOfSampleOrder4(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		marginTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH,
				"//div[@id='order_paper_margin']/h6")))
		paper_margin = self.driver.find_element_by_xpath("//div[@id='order_paper_margin']/h6").text
		self.assertEqual(str(paper_margin), "YES")
		paper_type = self.driver.find_element_by_xpath("//div[@id='order_paper_type']/h6").text
		self.assertEqual(str(paper_type), "Fuji")
		paper_surface = self.driver.find_element_by_xpath("//div[@id='order_paper_surface']/h6").text
		self.assertEqual(str(paper_surface), "glossy")
		paper_size = self.driver.find_element_by_xpath("//div[@id='order_paper_size']/h6").text
		self.assertEqual(str(paper_size), "4x6")
		paper_amount = self.driver.find_element_by_xpath("//div[@id='order_amount_picture']/h6").text
		self.assertEqual(str(paper_amount), "1 Picture")
		paper_price = self.driver.find_element_by_xpath("//div[@id='order_price_per_picture']/h6").text
		self.assertEqual(str(paper_price), "3 Baht")
		paper_price_total = self.driver.find_element_by_xpath("//div[@id='order_price_total']/h6").text
		self.assertEqual(str(paper_price_total), "3 Baht")

	def checkDetailOfSampleOrder5(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		marginTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH,
				"//div[@id='order_paper_margin']/h6")))
		paper_margin = self.driver.find_element_by_xpath("//div[@id='order_paper_margin']/h6").text
		self.assertEqual(str(paper_margin), "NO")
		paper_type = self.driver.find_element_by_xpath("//div[@id='order_paper_type']/h6").text
		self.assertEqual(str(paper_type), "Koduk Royal")
		paper_surface = self.driver.find_element_by_xpath("//div[@id='order_paper_surface']/h6").text
		self.assertEqual(str(paper_surface), "glossy")
		paper_size = self.driver.find_element_by_xpath("//div[@id='order_paper_size']/h6").text
		self.assertEqual(str(paper_size), "2P")
		paper_amount = self.driver.find_element_by_xpath("//div[@id='order_amount_picture']/h6").text
		self.assertEqual(str(paper_amount), "2 Picture")
		paper_price = self.driver.find_element_by_xpath("//div[@id='order_price_per_picture']/h6").text
		self.assertEqual(str(paper_price), "8 Baht")
		paper_price_total = self.driver.find_element_by_xpath("//div[@id='order_price_total']/h6").text
		self.assertEqual(str(paper_price_total), "16 Baht")

	def checkDetailOfSampleOrder6(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		marginTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH,
				"//div[@id='order_paper_margin']/h6")))
		paper_margin = self.driver.find_element_by_xpath("//div[@id='order_paper_margin']/h6").text
		self.assertEqual(str(paper_margin), "NO")
		paper_type = self.driver.find_element_by_xpath("//div[@id='order_paper_type']/h6").text
		self.assertEqual(str(paper_type), "Koduk Royal")
		paper_surface = self.driver.find_element_by_xpath("//div[@id='order_paper_surface']/h6").text
		self.assertEqual(str(paper_surface), "glossy")
		paper_size = self.driver.find_element_by_xpath("//div[@id='order_paper_size']/h6").text
		self.assertEqual(str(paper_size), "2P")
		paper_amount = self.driver.find_element_by_xpath("//div[@id='order_amount_picture']/h6").text
		self.assertEqual(str(paper_amount), "1 Picture")
		paper_price = self.driver.find_element_by_xpath("//div[@id='order_price_per_picture']/h6").text
		self.assertEqual(str(paper_price), "8 Baht")
		paper_price_total = self.driver.find_element_by_xpath("//div[@id='order_price_total']/h6").text
		self.assertEqual(str(paper_price_total), "8 Baht")

	def checkDetailOfSampleOrder2InOderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		statusTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH,
				"//div[@id='order-status']")))
		order_status = self.driver.find_element_by_xpath(
			"//div[@id='order-status']").text
		self.assertEqual(str(order_status), "Wait")
		order_surface = self.driver.find_element_by_xpath(
			"//div[@id='order-surface']").text
		self.assertEqual(str(order_surface), "glossy")
		order_type = self.driver.find_element_by_xpath(
			"//div[@id='order-type']").text
		self.assertEqual(str(order_type), "Koduk Royal")
		order_margin = self.driver.find_element_by_xpath(
			"//div[@id='order-margin']").text
		self.assertEqual(str(order_margin), "NO")
		order_size = self.driver.find_element_by_xpath(
			"//div[@id='order-size']").text
		self.assertEqual(str(order_size), "2P")
		order_price = self.driver.find_element_by_xpath(
			"//div[@id='order-price-per_picture']").text
		self.assertEqual(str(order_price), "8 Baht")
		order_amount = self.driver.find_element_by_xpath(
			"//div[@id='order-amount-picture']").text
		self.assertEqual(str(order_amount), "4 photos")
		order_price_total = self.driver.find_element_by_xpath(
			"//div[@id='order-price-total']").text
		self.assertEqual(str(order_price_total), "32 Baht")

	def checkDetailOfSampleOrder4InOderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		statusTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH,
				"//div[@id='order-status']")))
		order_status = self.driver.find_element_by_xpath(
			"//div[@id='order-status']").text
		self.assertEqual(str(order_status), "Wait")
		order_surface = self.driver.find_element_by_xpath(
			"//div[@id='order-surface']").text
		self.assertEqual(str(order_surface), "glossy")
		order_type = self.driver.find_element_by_xpath(
			"//div[@id='order-type']").text
		self.assertEqual(str(order_type), "Fuji")
		order_margin = self.driver.find_element_by_xpath(
			"//div[@id='order-margin']").text
		self.assertEqual(str(order_margin), "YES")
		order_size = self.driver.find_element_by_xpath(
			"//div[@id='order-size']").text
		self.assertEqual(str(order_size), "4x6")
		order_paper_size = self.driver.find_element_by_xpath(
			"//div[@id='order-price-per_picture']").text
		self.assertEqual(str(order_paper_size), "3 Baht")
		order_amount = self.driver.find_element_by_xpath(
			"//div[@id='order-amount-picture']").text
		self.assertEqual(str(order_amount), "1 photos")
		order_price_total = self.driver.find_element_by_xpath(
			"//div[@id='order-price-total']").text
		self.assertEqual(str(order_price_total), "3 Baht")

	def checkDetailOfSampleOrder2InOderExtendPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Check value
		imagenameDict = dict()
		imagenameDict[self.samplePicture1Name] = { "width" : "4064 px", "height" : "2704 px" }
		imagenameDict[self.samplePicture2Name] = { "width" : "960 px", "height" : "640 px" }
		imagenameDict[self.samplePicture3Name] = { "width" : "1227 px", "height" : "771 px" }
		imagenameDict[self.samplePicture4Name] = { "width" : "800 px", "height" : "600 px" }
		
		imagename1 = self.driver.find_element_by_xpath("(//div[@id='order-imagename'])[1]").text
		width1 = self.driver.find_element_by_xpath("(//div[@id='order-width'])[1]").text
		self.assertEqual(str(width1), imagenameDict[imagename1]["width"])
		height1 = self.driver.find_element_by_xpath("(//div[@id='order-height'])[1]").text
		self.assertEqual(str(height1), imagenameDict[imagename1]["height"])
			
		imagename2 = self.driver.find_element_by_xpath("(//div[@id='order-imagename'])[2]").text
		width2 = self.driver.find_element_by_xpath("(//div[@id='order-width'])[2]").text
		self.assertEqual(str(width2), imagenameDict[imagename2]["width"])
		height2 = self.driver.find_element_by_xpath("(//div[@id='order-height'])[2]").text
		self.assertEqual(str(height2), imagenameDict[imagename2]["height"])
			
		imagename3 = self.driver.find_element_by_xpath("(//div[@id='order-imagename'])[3]").text
		width3 = self.driver.find_element_by_xpath("(//div[@id='order-width'])[3]").text
		self.assertEqual(str(width3), imagenameDict[imagename3]["width"])
		height3 = self.driver.find_element_by_xpath("(//div[@id='order-height'])[3]").text
		self.assertEqual(str(height3), imagenameDict[imagename3]["height"])
			
		imagename4 = self.driver.find_element_by_xpath("(//div[@id='order-imagename'])[4]").text
		width4 = self.driver.find_element_by_xpath("(//div[@id='order-width'])[4]").text
		self.assertEqual(str(width4), imagenameDict[imagename4]["width"])
		height4 = self.driver.find_element_by_xpath("(//div[@id='order-height'])[4]").text
		self.assertEqual(str(height4), imagenameDict[imagename4]["height"])

		albumname1 = self.driver.find_element_by_xpath("(//div[@id='order-albumname'])[1]").text
		self.assertEqual(albumname1, self.album1Name)
		albumname2 = self.driver.find_element_by_xpath("(//div[@id='order-albumname'])[2]").text
		self.assertEqual(albumname2, self.album1Name)
		albumname3 = self.driver.find_element_by_xpath("(//div[@id='order-albumname'])[3]").text
		self.assertEqual(albumname3, self.album1Name)
		albumname4 = self.driver.find_element_by_xpath("(//div[@id='order-albumname'])[4]").text
		self.assertEqual(albumname4, self.album1Name)

	def checkDetailOfSampleOrder4InOderExtendPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Check value
		imagenameDict = dict()
		imagenameDict[self.samplePicture1Name] = { "width" : "4064 px", "height" : "2704 px" }
		
		imagename1 = self.driver.find_element_by_xpath("(//div[@id='order-imagename'])[1]").text
		width1 = self.driver.find_element_by_xpath("(//div[@id='order-width'])[1]").text
		self.assertEqual(str(width1), imagenameDict[imagename1]["width"])
		height1 = self.driver.find_element_by_xpath("(//div[@id='order-height'])[1]").text
		self.assertEqual(str(height1), imagenameDict[imagename1]["height"])
			
		albumname1 = self.driver.find_element_by_xpath("(//div[@id='order-albumname'])[1]").text
		self.assertEqual(albumname1, self.album1Name)

	def clickPictureInOrderInOrderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click Add album
		pictureButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Picture in order']")))
		pictureButtonElement.click()

	def clickConfirmOrderInOrderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click confirm order
		confirmOrderButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Confirm order']")))
		confirmOrderButtonElement.click()

	def clickConfirmedButtonInOrderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click confirm order
		confirmOrderButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='   Confirmed   ']")))
		confirmOrderButtonElement.click()

	def fromPictureInOrderPageBackToOrderPage(self):
		# Waitting for element was located
		backToOrderButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Back to Order']")))
		backToOrderButtonElement.click()

	def selectConfirmOrderSampleOrder4(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for find day paid element
		pictureNameElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, 
				"//select[@name='day_paid']/option[text()='31']")))
		self.driver.find_element_by_xpath(
			"//select[@name='day_paid']/option[text()='31']").click()
		self.driver.find_element_by_xpath(
			"//select[@name='month_paid']/option[text()='DEC']").click()
		self.driver.find_element_by_xpath(
			"//select[@name='year_paid']/option[text()='2017']").click()
		self.driver.find_element_by_xpath(
			"//select[@name='hour_paid']/option[text()='23']").click()
		self.driver.find_element_by_xpath(
			"//select[@name='minute_paid']/option[text()='59']").click()
		self.driver.find_element_by_name("cost").clear()
		self.driver.find_element_by_name("cost").send_keys("99.99")

	def clickConfirmOrderInConfirmOrderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click confirm order
		confirmOrderButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Confirm']")))
		confirmOrderButtonElement.click()

	def checkDetailOfSampleOrder4InConfirmOrderDetailPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		orderDateTimeTextElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH,
				"//div[@id='order_datetime']/h6")))
		datetimeConfirm = self.driver.find_element_by_xpath(
			"//div[@id='order_datetime']/h6").text
		self.assertEqual(str(datetimeConfirm), "2017-12-31 23:59")
		orderPaidAmount = self.driver.find_element_by_xpath(
			"//div[@id='order_paid_amount']/h6").text
		self.assertEqual(str(orderPaidAmount), "99.99")

# Admin
	def loginWithAdmin(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		usernameInputElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@name=\"username\"]")))
		# Input sample value
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys(self.adminUsername)
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys(self.adminPassword)
		self.driver.find_element_by_xpath("//input[@value='Login']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"HomePage"))

# Admin View MemberList
	def goToMemberListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		memberListLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//img[@src="/media/images/Member_List.png"]')))
		memberListLinkElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl("{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r'AdminViewMemberListPage'))

	def checkDetailOfSampleMember1InMemberList(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		memberListElement = self.wait.until(
			EC.presence_of_all_elements_located((
				By.ID, "member")))
		# Input sample value
		# member_list = self.driver.find_elements_by_id("member")
		is_found = False
		for index, member in enumerate(memberListElement,0):
			username = member.find_element_by_id("member_username").text
			if not username == self.memberUsername:
				continue
			firstname = member.find_element_by_id("member_firstname").text
			self.assertEqual(firstname, self.memberFirstname)
			lastname = member.find_element_by_id("member_lastname").text
			self.assertEqual(lastname, self.memberLastname)
			email = member.find_element_by_id("member_email").text
			self.assertEqual(str(email), self.memberEmail)
			permission = member.find_element_by_id("member_permission").text
			self.assertEqual(str(permission), self.memberPermission)
			is_found = True
			break
		if not is_found:
			raise Photo2Print_TestCaseException( "Sample member 1 was not found in member page." )

	def checkDetailOfSampleMember2InMemberList(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		memberListElement = self.wait.until(
			EC.presence_of_all_elements_located((
				By.ID, "member")))
		# Input sample value
		# member_list = self.driver.find_elements_by_id("member")
		is_found = False
		for index, member in enumerate(memberListElement,0):
			username = member.find_element_by_id("member_username").text
			if not username == self.memberNewUsername:
				continue
			firstname = member.find_element_by_id("member_firstname").text
			self.assertEqual(firstname, self.memberNewFirstname)
			lastname = member.find_element_by_id("member_lastname").text
			self.assertEqual(lastname, self.memberNewLastname)
			email = member.find_element_by_id("member_email").text
			self.assertEqual(str(email), self.memberNewEmail)
			permission = member.find_element_by_id("member_permission").text
			self.assertEqual(str(permission), self.memberNewPermission)
			is_found = True
			break
		if not is_found:
			raise Photo2Print_TestCaseException( "Sample member 2 was not found in member page." )

# Admin delete user
	def clickToDeleteSampleMember1InMemberListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		removeButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u"//form[input/@value='{}']/input[@value='Remove']".format(
					self.memberUsername))))
		removeButtonElement.click()
		self.busyLoopWaitAlert()
		self.assertRegexpMatches(
			self.closeAlertAndGetItsText(),
			u'Are you sure to delete this User?')
		time.sleep(self.waittingTime)

	def clickToDeleteSampleMember2InMemberListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		removeButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u"//form[input/@value='{}']/input[@value='Remove']".format(
					self.memberNewUsername))))
		removeButtonElement.click()
		self.busyLoopWaitAlert()
		self.assertRegexpMatches(
			self.closeAlertAndGetItsText(),
			u'Are you sure to delete this User?')
		time.sleep(self.waittingTime)

	def deleteSampleMember1WithAdmin(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		usernameInputelement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@name=\"username\"]")))
		# input admin detail
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys(self.adminUsername)
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys(self.adminPassword)
		self.driver.find_element_by_xpath("//input[@value='Login']").click()
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		memberListLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//img[@src="/media/images/Member_List.png"]')))
		memberListLinkElement.click()
		# Waitting for element was located
		removeSmapleMember1ButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//form[input/@value='" + self.memberUsername + "']/input[@value='Remove']")))
		removeSmapleMember1ButtonElement.click()
		# Waitting for Alert
		self.busyLoopWaitAlert()
		self.assertRegexpMatches(
			self.closeAlertAndGetItsText(),
			u'Are you sure to delete this User?')
		# Waitting for Delete user
		time.sleep(self.reloadWaittingTime)
		
	def deleteSampleMember2WithAdmin(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		usernameInputelement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//input[@name=\"username\"]")))
		# input admin detail
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"username\"]").send_keys(self.adminUsername)
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").clear()
		self.driver.find_element_by_xpath("//input[@name=\"password\"]").send_keys(self.adminPassword)
		self.driver.find_element_by_xpath("//input[@value='Login']").click()
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		memberListLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//img[@src="/media/images/Member_List.png"]')))
		memberListLinkElement.click()
		# Waitting for element was located
		removeSmapleMember1ButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//form[input/@value='" + self.memberNewUsername + "']/input[@value='Remove']")))
		removeSmapleMember1ButtonElement.click()
		# Waitting for Alert
		self.busyLoopWaitAlert()
		self.assertRegexpMatches(
			self.closeAlertAndGetItsText(),
			u'Are you sure to delete this User?')
		# Waitting for Delete user
		time.sleep(self.reloadWaittingTime)

# Admin Edit user
	def clickToEditSampleMember1InMemberListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		editMember1ButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				"//form[input/@value='{}']/input[@value='Edit']".format(
					self.memberUsername
				))))
		editMember1ButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"EditUserDetail",
			self.memberUsername))

	def adminEditDetailToSampleMember2(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		usernameInputelement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input new user detail
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath(
			"//input[@type='radio' and @value='"+self.memberNewPermissionChoice+"']").click()
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"AdminViewMemberListPage"))

	def adminEditDetailToSampleMember2ButEmptyUserName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		usernameInputelement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input new user detail
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys("")
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath(
			"//input[@type='radio' and @value='"+self.memberNewPermissionChoice+"']").click()
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

	def adminEditDetailToSampleMember2ButEmptyFirstName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		usernameInputelement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input new user detail
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys("")
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath(
			"//input[@type='radio' and @value='"+self.memberNewPermissionChoice+"']").click()
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

	def adminEditDetailToSampleMember2ButEmptyLastName(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		usernameInputelement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input new user detail
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys("")
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath(
			"//input[@type='radio' and @value='"+self.memberNewPermissionChoice+"']").click()
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

	def adminEditDetailToSampleMember2ButEmptyEmail(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		usernameInputelement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input new user detail
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys("")
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword)
		self.driver.find_element_by_xpath(
			"//input[@type='radio' and @value='"+self.memberNewPermissionChoice+"']").click()
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

	def adminEditDetailToSampleMember2ButEmptyPassword(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		usernameInputelement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input new user detail
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys("")
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys("")
		self.driver.find_element_by_xpath(
			"//input[@type='radio' and @value='"+self.memberNewPermissionChoice+"']").click()
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

	def adminEditDetailToSampleMember2ButWrongConfirmPassword(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		usernameInputelement = self.wait.until(
			EC.visibility_of_element_located((
				By.NAME, "username")))
		# Input new user detail
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.memberNewUsername)
		self.driver.find_element_by_name("firstname").clear()
		self.driver.find_element_by_name("firstname").send_keys(self.memberNewFirstname)
		self.driver.find_element_by_name("lastname").clear()
		self.driver.find_element_by_name("lastname").send_keys(self.memberNewLastname)
		self.driver.find_element_by_name("e_mail").clear()
		self.driver.find_element_by_name("e_mail").send_keys(self.memberNewEmail)
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.memberNewPassword)
		self.driver.find_element_by_name("password_c").clear()
		self.driver.find_element_by_name("password_c").send_keys(self.memberNewPassword + "r")
		self.driver.find_element_by_xpath(
			"//input[@type='radio' and @value='"+self.memberNewPermissionChoice+"']").click()
		self.driver.find_element_by_xpath("//input[@value='save']").click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/".format(
			self.base_url,
			r"EditUserDetailFunction"))

# Admin View member album
	def clickToAlbumSampleMember1InMemberListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		albumOfMember1ButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				"//form[input/@value='{}']/input[@value='Albums']".format(
					self.memberUsername))))
		albumOfMember1ButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"AdminViewMemberAlbums",
			self.memberUsername))

	def checkDetailOfSampleAlbum1InMemberAlbumPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)

		albumnameDict = dict()
		albumnameDict[self.album1Name] = { "permission" : "Private" }
		albumnameDict[self.album2Name] = { "permission" : "Public" }
		
		albumname1 = self.driver.find_element_by_xpath("(//div[@id='albumname'])[1]").text
		permission1 = self.driver.find_element_by_xpath("(//div[@id='permission'])[1]").text
		self.assertEqual(str(permission1), albumnameDict[albumname1]["permission"])
			
		albumname2 = self.driver.find_element_by_xpath("(//div[@id='albumname'])[2]").text
		permission2 = self.driver.find_element_by_xpath("(//div[@id='permission'])[2]").text
		self.assertEqual(str(permission2), albumnameDict[albumname2]["permission"])


# Admin View member picture
	def clickToImagesOfSampleAlbum1InAlbumListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		imageOfAlbum1ButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u"//form[input/@value='{}']/input[@value='Images']".format(
					self.album1Name))))
		imageOfAlbum1ButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"AdminViewMemberPicture",
			self.memberUsername,
			self.album1Name))

	def clickToImagesOfSampleAlbum2InAlbumListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		imageOfAlbum2ButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u"//form[input/@value='{}']/input[@value='Images']".format(
					self.album2Name))))
		imageOfAlbum2ButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"AdminViewMemberPicture",
			self.memberUsername,
			self.album2Name))

	def checkDetailPicturesOfSampleAlbum1InMemberPicturePage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		imagenameDict = dict()
		imagenameDict[self.samplePicture1Name] = {
			"width" : "4064 px", "height" : "2704 px", "albumname" : self.album1Name }
		imagenameDict[self.samplePicture2Name] = {
			"width" : "960 px", "height" : "640 px", "albumname" : self.album1Name }
		
		imagename1 = self.driver.find_element_by_xpath("(//div[@id='imagename'])[1]").text
		albumname1 = self.driver.find_element_by_xpath("(//div[@id='albumname'])[1]").text
		self.assertEqual(albumname1, imagenameDict[imagename1]["albumname"])
		width1 = self.driver.find_element_by_xpath("(//div[@id='width'])[1]").text
		self.assertEqual(str(width1), imagenameDict[imagename1]["width"])
		height1 = self.driver.find_element_by_xpath("(//div[@id='height'])[1]").text
		self.assertEqual(str(height1), imagenameDict[imagename1]["height"])
			
		imagename2 = self.driver.find_element_by_xpath("(//div[@id='imagename'])[2]").text
		albumname2 = self.driver.find_element_by_xpath("(//div[@id='albumname'])[2]").text
		self.assertEqual(albumname2, imagenameDict[imagename2]["albumname"])
		width2 = self.driver.find_element_by_xpath("(//div[@id='width'])[2]").text
		self.assertEqual(str(width2), imagenameDict[imagename2]["width"])
		height2 = self.driver.find_element_by_xpath("(//div[@id='height'])[2]").text
		self.assertEqual(str(height2), imagenameDict[imagename2]["height"])

	def checkDetailPicturesOfSampleAlbum2InMemberPicturePage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		imagenameDict = dict()
		imagenameDict[self.samplePicture3Name] = {
			"width" : "1227 px", "height" : "771 px", "albumname" : self.album2Name }
		imagenameDict[self.samplePicture4Name] = {
			"width" : "800 px", "height" : "600 px", "albumname" : self.album2Name }
		
		imagename1 = self.driver.find_element_by_xpath("(//div[@id='imagename'])[1]").text
		albumname1 = self.driver.find_element_by_xpath("(//div[@id='albumname'])[1]").text
		self.assertEqual(albumname1, imagenameDict[imagename1]["albumname"])
		width1 = self.driver.find_element_by_xpath("(//div[@id='width'])[1]").text
		self.assertEqual(str(width1), imagenameDict[imagename1]["width"])
		height1 = self.driver.find_element_by_xpath("(//div[@id='height'])[1]").text
		self.assertEqual(str(height1), imagenameDict[imagename1]["height"])
			
		imagename2 = self.driver.find_element_by_xpath("(//div[@id='imagename'])[2]").text
		albumname2 = self.driver.find_element_by_xpath("(//div[@id='albumname'])[2]").text
		self.assertEqual(albumname2, imagenameDict[imagename2]["albumname"])
		width2 = self.driver.find_element_by_xpath("(//div[@id='width'])[2]").text
		self.assertEqual(str(width2), imagenameDict[imagename2]["width"])
		height2 = self.driver.find_element_by_xpath("(//div[@id='height'])[2]").text
		self.assertEqual(str(height2), imagenameDict[imagename2]["height"])

	def fromMemberPicturePageBackToMemberAlbumPage(self):
		# Checking current url is correct
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			u'AdminViewMemberPicture',
			self.memberUsername,
			self.album1Name))
		# Waitting for element was located
		backToAlbumListButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Back to Album list']")))
		backToAlbumListButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			u'AdminViewMemberAlbums',
			self.memberUsername))

# Admin Order Album
	def clickToOrderSampleAlbum1InAlbumListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		imageOfAlbum1ButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u"//form[input/@value='{}']/input[@value='Add this album to print']".format(
					self.album1Name))))
		imageOfAlbum1ButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"OrderPage",
			r"AlbumName",
			self.album1Name,
			r"MemberSelectPaperTypePage",
			self.memberUsername))

	def checkDetailOrderOfSampleOrder5InMemberOrderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		status1 = self.driver.find_element_by_xpath("(//div[@id='order-status']/a)[1]").text
		self.assertEqual(status1, "Wait")
		surface1 = self.driver.find_element_by_xpath("(//div[@id='order-surface']/a)[1]").text
		self.assertEqual(surface1, "glossy")
		type1 = self.driver.find_element_by_xpath("(//div[@id='order-type']/a)[1]").text
		self.assertEqual(type1, "Koduk Royal")
		margin1 = self.driver.find_element_by_xpath("(//div[@id='order-margin']/a)[1]").text
		self.assertEqual(margin1, "NO")
		size1 = self.driver.find_element_by_xpath("(//div[@id='order-size']/a)[1]").text
		self.assertEqual(size1, "2P")
		price1 = self.driver.find_element_by_xpath("(//div[@id='order-price'])[1]").text
		self.assertEqual(price1, u"16 Baht")
		checked1 = self.driver.find_element_by_xpath("(//div[@id='order-paid-checked'])[1]").text
		self.assertEqual(checked1, "False")
		confirm1 = self.driver.find_element_by_xpath("(//div[@id='order-is-confirm'])[1]").text
		self.assertEqual(confirm1, "False")

# Admin Order Picture
	def clickToOrderSamplePicture1InPictureListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		imageOfAlbum1ButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u"//form[input/@value='{}']/input[@value='Add this image to print']".format(
					self.samplePicture1Name))))
		imageOfAlbum1ButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"OrderPage",
			r"AlbumName",
			self.album1Name,
			r"PictureName",
			self.samplePicture1Name,
			r"MemberSelectPaperTypePage",
			self.memberUsername))

	def checkDetailOrderOfSampleOrder6InMemberOrderPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		#####CHECK VALUE ARE MATCH
		status1 = self.driver.find_element_by_xpath("(//div[@id='order-status']/a)[1]").text
		self.assertEqual(status1, "Wait")
		surface1 = self.driver.find_element_by_xpath("(//div[@id='order-surface']/a)[1]").text
		self.assertEqual(surface1, "glossy")
		type1 = self.driver.find_element_by_xpath("(//div[@id='order-type']/a)[1]").text
		self.assertEqual(type1, "Koduk Royal")
		margin1 = self.driver.find_element_by_xpath("(//div[@id='order-margin']/a)[1]").text
		self.assertEqual(margin1, "NO")
		size1 = self.driver.find_element_by_xpath("(//div[@id='order-size']/a)[1]").text
		self.assertEqual(size1, "2P")
		price1 = self.driver.find_element_by_xpath("(//div[@id='order-price'])[1]").text
		self.assertEqual(price1, u"8 Baht")
		checked1 = self.driver.find_element_by_xpath("(//div[@id='order-paid-checked'])[1]").text
		self.assertEqual(checked1, "False")
		confirm1 = self.driver.find_element_by_xpath("(//div[@id='order-is-confirm'])[1]").text
		self.assertEqual(confirm1, "False")

# Admin View Order
	def goToOrderListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		memberListLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, '//img[@src="/media/images/Order_List.png"]')))
		memberListLinkElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl("{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r'AdminViewAllMemberOrder'))


	def checkDetailOfSampleOrder4InOderListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		orderListElement = self.wait.until(
			EC.presence_of_all_elements_located((
				By.ID, "order")))
		# Input sample value
		is_found = False
		for index, order in enumerate(orderListElement,0):
			username = order.find_element_by_id("order-username").text
			if not username == self.memberUsername:
				continue
			order_username = order.find_element_by_id("order-username").text
			self.assertEqual(order_username, self.memberUsername)
			order_status = order.find_element_by_id("order-status").text
			self.assertEqual(order_status, "Wait")
			order_surface = order.find_element_by_id("order-surface").text
			self.assertEqual(order_surface, "glossy")
			order_type = order.find_element_by_id("order-type").text
			self.assertEqual(order_type, "Fuji")
			order_margin = order.find_element_by_id("order-margin").text
			self.assertEqual(order_margin, "YES")
			order_size = order.find_element_by_id("order-size").text
			self.assertEqual(order_size, "4x6")
			order_price = order.find_element_by_id("order-price").text
			self.assertEqual(order_price, "3 Baht")
			order_paid = order.find_element_by_id("order-paid-checked").text
			self.assertEqual(order_paid, "False")
			is_found = True
			break
		if not is_found:
			raise Photo2Print_TestCaseException( "Sample order 4 was not found in order list page." )

# Admin Download order
	def checkDetailOfSampleOrder2InOderListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		orderListElement = self.wait.until(
			EC.presence_of_all_elements_located((
				By.ID, "order")))
		# Input sample value
		is_found = False
		for index, order in enumerate(orderListElement,0):
			username = order.find_element_by_id("order-username").text
			if not username == self.memberUsername:
				continue
			order_username = order.find_element_by_id("order-username").text
			self.assertEqual(order_username, self.memberUsername)
			order_status = order.find_element_by_id("order-status").text
			self.assertEqual(order_status, "Wait")
			order_surface = order.find_element_by_id("order-surface").text
			self.assertEqual(order_surface, "glossy")
			order_type = order.find_element_by_id("order-type").text
			self.assertEqual(order_type, "Koduk Royal")
			order_margin = order.find_element_by_id("order-margin").text
			self.assertEqual(order_margin, "NO")
			order_size = order.find_element_by_id("order-size").text
			self.assertEqual(order_size, "2P")
			order_price = order.find_element_by_id("order-price").text
			self.assertEqual(order_price, "32 Baht")
			is_found = True
			break
		if not is_found:
			raise Photo2Print_TestCaseException( "Sample order 2 was not found in order list page." )

	def clickToDownloadSampleOrder2InOrderListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		datetime_in = self.driver.find_element_by_xpath(
			"//form[input/@value='" + self.memberUsername + "']/input[@name='datetime']").get_attribute("value")
		datetime_object_in = dateutil.parser.parse(datetime_in)
		self.downloadFileName = \
			"{}_{}.zip".format(
				self.memberUsername, datetime_object_in.strftime("%Y-%m-%dT%H_%M_%S"))
		self.driver.find_element_by_xpath(
			"//form[input/@value='" + self.memberUsername + "']/input[@value='Download']").click()
		time.sleep(self.waittingTime)
		self.busyLoopWaitDownloadOrder()
		
	def checkFileInDownloadSampleOrder2(self):
		imagenameList = [
			self.samplePicture1Name,
			self.samplePicture2Name,
			self.samplePicture3Name,
			self.samplePicture4Name]
		downloadPath = os.path.abspath(
			os.path.join(os.path.dirname( __file__ ), 'download_dir',self.downloadFileName))
		file_object = open(downloadPath, 'r+')
		zip_object = zipfile.ZipFile(file_object)
		filename_list = []
		for filePath in zip_object.namelist():
			(dirname, filename) = os.path.split(filePath)
			filename_list.append(filename)
			zip_object.extract(filename, os.path.join(downloadPath,".."))
		file_object.close()
		os.remove(downloadPath)
		extrackNumber = 0
		for filename in filename_list:
			extrackNumber += 1
			if not filename in imagenameList:
				raise Photo2Print_TestCaseException( 
					"Not have imagefile in zip file" )
			os.remove(os.path.join(downloadPath,"..", filename))
		self.assertEqual(
			extrackNumber,
			len(filename_list))

# Admin edit order status
	def clickToEditStatusSampleOrder2InOrderListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Get datetime order
		datetime_order_in = self.driver.find_element_by_xpath(
			"//form[input/@value='" + self.memberUsername + "']/input[@name='datetime']").get_attribute("value")
		datetime_object_in = dateutil.parser.parse(datetime_order_in)
		datetime_order_with_timezone = \
			"{}+07:00".format(
				datetime_object_in.strftime("%Y-%m-%dT%H:%M:%S"))
		# Waitting for element was located
		editStatusSampleOrder2ButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, 
				u"//form[input/@value='{}']/input[@value='Edit Status']".format(
					self.memberUsername))))
		editStatusSampleOrder2ButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"AdminEditOrderStatusPage",
			self.memberUsername,
			datetime_order_with_timezone))

	def selectOrderStatusToWaitInOrderStatusPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click type choice
		waitStatusRadioElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='W']")))
		waitStatusRadioElement.click()

	def selectOrderStatusToProcessInOrderStatusPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click type choice
		processStatusRadioElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='P']")))
		processStatusRadioElement.click()

	def selectOrderStatusToSuccessInOrderStatusPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click type choice
		successStatusRadioElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='S']")))
		successStatusRadioElement.click()

	def selectOrderStatusToCancelInOrderStatusPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click type choice
		cancelStatusRadioElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH,
				"//input[@type='radio' and @value='E']")))
		cancelStatusRadioElement.click()

	def clickSaveInEditOrderStatusPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waittin for click Add album
		saveButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Save']")))
		saveButtonElement.click()

	def checkDetailOfSampleOrder2InOderListPage_StatusWait(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		orderListElement = self.wait.until(
			EC.presence_of_all_elements_located((
				By.ID, "order")))
		# Input sample value
		is_found = False
		for index, order in enumerate(orderListElement,0):
			username = order.find_element_by_id("order-username").text
			if not username == self.memberUsername:
				continue
			order_username = order.find_element_by_id("order-username").text
			self.assertEqual(order_username, self.memberUsername)
			order_status = order.find_element_by_id("order-status").text
			self.assertEqual(order_status, "Wait")
			order_surface = order.find_element_by_id("order-surface").text
			self.assertEqual(order_surface, "glossy")
			order_type = order.find_element_by_id("order-type").text
			self.assertEqual(order_type, "Koduk Royal")
			order_margin = order.find_element_by_id("order-margin").text
			self.assertEqual(order_margin, "NO")
			order_size = order.find_element_by_id("order-size").text
			self.assertEqual(order_size, "2P")
			order_price = order.find_element_by_id("order-price").text
			self.assertEqual(order_price, "32 Baht")
			is_found = True
			break
		if not is_found:
			raise Photo2Print_TestCaseException( "Sample order 2 was not found in order list page." )

	def checkDetailOfSampleOrder2InOderListPage_StatusProcess(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		orderListElement = self.wait.until(
			EC.presence_of_all_elements_located((
				By.ID, "order")))
		# Input sample value
		is_found = False
		for index, order in enumerate(orderListElement,0):
			username = order.find_element_by_id("order-username").text
			if not username == self.memberUsername:
				continue
			order_username = order.find_element_by_id("order-username").text
			self.assertEqual(order_username, self.memberUsername)
			order_status = order.find_element_by_id("order-status").text
			self.assertEqual(order_status, "Process")
			order_surface = order.find_element_by_id("order-surface").text
			self.assertEqual(order_surface, "glossy")
			order_type = order.find_element_by_id("order-type").text
			self.assertEqual(order_type, "Koduk Royal")
			order_margin = order.find_element_by_id("order-margin").text
			self.assertEqual(order_margin, "NO")
			order_size = order.find_element_by_id("order-size").text
			self.assertEqual(order_size, "2P")
			order_price = order.find_element_by_id("order-price").text
			self.assertEqual(order_price, "32 Baht")
			is_found = True
			break
		if not is_found:
			raise Photo2Print_TestCaseException( "Sample order 2 was not found in order list page." )
	def checkDetailOfSampleOrder2InOderListPage_StatusSuccess(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		orderListElement = self.wait.until(
			EC.presence_of_all_elements_located((
				By.ID, "order")))
		# Input sample value
		is_found = False
		for index, order in enumerate(orderListElement,0):
			username = order.find_element_by_id("order-username").text
			if not username == self.memberUsername:
				continue
			order_username = order.find_element_by_id("order-username").text
			self.assertEqual(order_username, self.memberUsername)
			order_status = order.find_element_by_id("order-status").text
			self.assertEqual(order_status, "Success")
			order_surface = order.find_element_by_id("order-surface").text
			self.assertEqual(order_surface, "glossy")
			order_type = order.find_element_by_id("order-type").text
			self.assertEqual(order_type, "Koduk Royal")
			order_margin = order.find_element_by_id("order-margin").text
			self.assertEqual(order_margin, "NO")
			order_size = order.find_element_by_id("order-size").text
			self.assertEqual(order_size, "2P")
			order_price = order.find_element_by_id("order-price").text
			self.assertEqual(order_price, "32 Baht")
			is_found = True
			break
		if not is_found:
			raise Photo2Print_TestCaseException( "Sample order 2 was not found in order list page." )
	def checkDetailOfSampleOrder2InOderListPage_StatusCancel(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		orderListElement = self.wait.until(
			EC.presence_of_all_elements_located((
				By.ID, "order")))
		# Input sample value
		is_found = False
		for index, order in enumerate(orderListElement,0):
			username = order.find_element_by_id("order-username").text
			if not username == self.memberUsername:
				continue
			order_username = order.find_element_by_id("order-username").text
			self.assertEqual(order_username, self.memberUsername)
			order_status = order.find_element_by_id("order-status").text
			self.assertEqual(order_status, "Cancel")
			order_surface = order.find_element_by_id("order-surface").text
			self.assertEqual(order_surface, "glossy")
			order_type = order.find_element_by_id("order-type").text
			self.assertEqual(order_type, "Koduk Royal")
			order_margin = order.find_element_by_id("order-margin").text
			self.assertEqual(order_margin, "NO")
			order_size = order.find_element_by_id("order-size").text
			self.assertEqual(order_size, "2P")
			order_price = order.find_element_by_id("order-price").text
			self.assertEqual(order_price, "32 Baht")
			is_found = True
			break
		if not is_found:
			raise Photo2Print_TestCaseException( "Sample order 2 was not found in order list page." )

	def checkOrderStatusRespondMustSelectChoice(self):
		# Waittin for respond element located
		respondElement = self.wait.until(
			EC.visibility_of_element_located((
				By.XPATH, "//div[@id='respond']/p")))
		self.assertEqual(str(respondElement.text), "Must select some choice.")

# Admin view confirm order
	def clickToViewConfirmDetailSampleOrder4InOrderListPage(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Get datetime order
		datetime_order_in = self.driver.find_element_by_xpath(
			"//form[input/@value='" + self.memberUsername + "']/input[@name='datetime']").get_attribute("value")
		datetime_object_in = dateutil.parser.parse(datetime_order_in)
		datetime_order_with_timezone = \
			"{}+07:00".format(
				datetime_object_in.strftime("%Y-%m-%dT%H:%M:%S"))
		# Waitting for element was located
		memberListLinkElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//form[input/@value='{}']/input[@value='Confirm Detail']".format(
					self.memberUsername
				))))
		memberListLinkElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"AdminViewMemberConfirmOrderDetail",
			self.memberUsername,
			datetime_order_with_timezone))
# Admin Confirm Order Paid
	def clickPaidCheckedSampleOrder4InConfirmOrderDetailPage(self):
				# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element was located
		paidCheckedButtonElement = self.wait.until(
			EC.element_to_be_clickable((
				By.XPATH, "//input[@value='Paid Checked']")))
		paidCheckedButtonElement.click()
		# Waitting url for change
		self.busyLoopWaitUrl(u"{}/{}/{}/{}/".format(
			self.base_url,
			self.adminUsername,
			r"AdminViewMemberOrder",
			self.memberUsername))

	def checkDetailOfSampleOrder4InOderListPage_PaidChecked(self):
		# Waitting for ssl refeed
		time.sleep(self.reloadWaittingTime)
		# Waitting for element
		orderListElement = self.wait.until(
			EC.presence_of_all_elements_located((
				By.ID, "order")))
		# Input sample value
		is_found = False
		for index, order in enumerate(orderListElement,0):
			username = order.find_element_by_id("order-username").text
			if not username == self.memberUsername:
				continue
			order_username = order.find_element_by_id("order-username").text
			self.assertEqual(order_username, self.memberUsername)
			order_status = order.find_element_by_id("order-status").text
			self.assertEqual(order_status, "Wait")
			order_surface = order.find_element_by_id("order-surface").text
			self.assertEqual(order_surface, "glossy")
			order_type = order.find_element_by_id("order-type").text
			self.assertEqual(order_type, "Fuji")
			order_margin = order.find_element_by_id("order-margin").text
			self.assertEqual(order_margin, "YES")
			order_size = order.find_element_by_id("order-size").text
			self.assertEqual(order_size, "4x6")
			order_price = order.find_element_by_id("order-price").text
			self.assertEqual(order_price, "3 Baht")
			order_paid = order.find_element_by_id("order-paid-checked").text
			self.assertEqual(order_paid, "True")
			is_found = True
			break
		if not is_found:
			raise Photo2Print_TestCaseException( "Sample order 4 was not found in order list page." )

