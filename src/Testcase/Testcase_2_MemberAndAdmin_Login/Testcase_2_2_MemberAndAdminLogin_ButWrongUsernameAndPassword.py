# !/usr/bin/python
# -*- coding: utf-8 -*-

'''
    Developer Name
        Anan (Nun) Kamkemkeaw
    Company Name
        king mongkut's university of technology north bangkok
'''
import os.path
import sys
parent = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, parent)

import unittest
from selenium import webdriver
from Photo2Print_TestCaseHandler import Photo2PrintTestCaseHandler

class Testcase_2_2_Login(Photo2PrintTestCaseHandler):

	def testcase_2_2_login(self):
		# Set WebDrive to firefox
		self.setWebDrive(webdriver.Firefox())
		# Register with sample member number 1
		self.registerWithSampleMember1()
		# Check register was success
		self.checkRegisterRespondSuccess()
		# Test login with sample member number 1
		self.loginWithSampleMember1()
		# Logout
		self.allUserLogout()
		# Try login with sample member number 1 but wrong input username
		self.loginWithSampleMember1ButNotWrongUserName()
		# Check error respond was correct
		self.checkLoginRespondWrongUserNameOrPassword()
		# Try login with sample member number 1 but password input username
		self.loginWithSampleMember1ButNotWrongPassword()
		# Check error respond was correct
		self.checkLoginRespondWrongUserNameOrPassword()
		# Delete sample member number 1
		self.deleteSampleMember1WithAdmin()

if __name__ == "__main__":
	unittest.main()